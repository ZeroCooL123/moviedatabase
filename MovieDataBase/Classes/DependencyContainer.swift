//
//  DependencyContainer.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import LocalAuthentication

class DependencyContainer {
    
    let networkService: NetworkManager
    let localService: LocalManager
    let defaults: DefaultsManager
    let biometrics: BiometricManager
    
    init(networkService: NetworkManager = NetworkManager(), localService: LocalManager = LocalManager(), defaults: DefaultsManager = DefaultsManager(defaults: UserDefaults.standard), biometrics: BiometricManager = BiometricManager()) {
        self.networkService = networkService
        self.localService = localService
        self.defaults = defaults
        self.biometrics = biometrics
    }
}


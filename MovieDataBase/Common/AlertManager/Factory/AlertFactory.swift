//
//  AlertFactory.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

protocol AlertFactory {
    func makeAlertView() -> AlertView
    func makeAlertVC(_ view: AlertView, coordinator: MainCoordinator, type: AlertType, content: AlertContent?) -> AlertVC
}

extension DependencyContainer: AlertFactory {
    
    func makeAlertView() -> AlertView {
        let view = AlertView()
        
        return view
    }
    
    func makeAlertVC(_ view: AlertView, coordinator: MainCoordinator, type: AlertType, content: AlertContent?) -> AlertVC {
        let vc = AlertVC(UI: view, factory: self, type: type, content: content, biometrics: biometrics)
        vc.modalPresentationStyle = .overCurrentContext
        
        return vc
    }

}


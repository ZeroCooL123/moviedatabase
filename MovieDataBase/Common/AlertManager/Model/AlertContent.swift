//
//  AlertContent.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

struct AlertContent {
    
    let icon: UIImage?
    let title: String?
    let description: String?
    let acctionButtonTitle: String?
    let cancelButtonTitle: String?
}


//
//  AlertVC.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import UIKit

class AlertVC: UIViewController {

    // MARK: - Controller's properties
    
    var fallbackAction: (() -> Void)?
    var acceptAction: (() -> Void)?
    var UI: AlertView?
    var content: AlertContent?
    let biometrics: BiometricManager
    private var factory: AlertFactory
    
    // MARK: - Controller's initializers
    
    required init(UI: AlertView, factory: AlertFactory, type: AlertType, content: AlertContent?, biometrics: BiometricManager) {
        
        self.factory = factory
        self.biometrics = biometrics
        self.content = content
        
        switch type {
        case .Single:
            self.UI = SingleAlertView(content: self.content)
        case .Multiple:
            self.UI = MultipleAlertView(content: self.content)
        default:
            self.UI = BiometricsAlertView(biometrics: biometrics)
        }
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Controller's life cycle
    
    override func loadView() {
        self.view = self.UI
    }
    
    override func viewDidLayoutSubviews() {}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }
    
}

// MARK: - Controller's methods

extension AlertVC {
    
    func addFallbackAction(_ actionTitle: String?, action: (() -> Void)?) {
        self.UI?.fallbackButton?.setTitle(actionTitle, for: .normal)
        self.fallbackAction = action
    }
    
    func addAcceptAction(_ actionTitle: String?, action: (() -> Void)?) {
        self.UI?.continueButton?.setTitle(actionTitle, for: .normal)
        self.acceptAction = action
    }
    
    private func setDelegates() {
        self.UI?.delegate = self
    }
}

// MARK: - Controller's view delegate methods

extension AlertVC: AlertViewDelegate {
    
    func didAcceptButtonTap(_ view: AlertView) {
        dismiss(animated: false, completion: nil)
        acceptAction?()
    }
    
    func didCancelButtonTap(_ view: AlertView) {
        dismiss(animated: false, completion: nil)
        fallbackAction?()
    }
    
    func didCloseButtonTap(_ view: AlertView) {
        dismiss(animated: false, completion: nil)
    }
    
}

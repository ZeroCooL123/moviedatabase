//
//  AlertView.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

enum AlertType {
    case Single
    case Multiple
    case Biometrics
}

protocol AlertViewDelegate: NSObjectProtocol {
    func didCloseButtonTap(_ view: AlertView)
    func didAcceptButtonTap(_ view: AlertView)
    func didCancelButtonTap(_ view: AlertView)
}

class AlertView: UIView {
    
    private var firstButton: UIButton?
    private var secondButton: UIButton?
    
    weak var delegate: AlertViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setFallbackButton(_ button: UIButton) {
        firstButton = button
    }
    
    func setAcceptButton(_ button: UIButton) {
        secondButton = button
    }
    
    var fallbackButton: UIButton? { return firstButton }
    var continueButton: UIButton? { return secondButton }
    
}





//
//  BiometricsAlertView.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import UIKit

class BiometricsAlertView: AlertView {

    var compactConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var regularConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var largeConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    
    static let shared = AlertView()
    var parentView: UIView!
    
    let alertContainer: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .white
        view.layer.cornerRadius = 10.0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let alertTitle: UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.text = "Accede a SUSTAIN"
        label.textColor = .gray
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let acceptButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.backgroundColor = UIColor.clear
        button.addTarget(self, action: #selector(didAcceptButtonPress), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false

        return button
    }()
    
    let alertMessage: UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.text = ""
        label.textColor = .gray
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.setTitle("Usar NIP", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 10.0
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(didCancelButtonPress), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false

        return button
    }()
    
    let stack: UIStackView = {
        let stack = UIStackView(frame: .zero)
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 10
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initComponents()
    }
    
    required init(biometrics: BiometricManager) {
        self.init()
        
        biometrics.getBiometricType { (biometricType) in
            switch biometricType {
            case .face:
                self.alertMessage.text = "Escanea tu rostro"
                self.acceptButton.setImage(UIImage(named: "faceidIcon"), for: .normal)
            case .touch:
                self.alertMessage.text = "Escanea tu huella digital"
                self.acceptButton.setImage(UIImage(named: "fingerprintIcon"), for: .normal)
            default:
                self.acceptButton.setImage(UIImage(), for: .normal)
            }
        }

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initComponents() {
        self.addComponents()
        self.setAutolayout()
        self.activateCurrentLayout()
        self.setFallbackButton(cancelButton)
        self.setAcceptButton(acceptButton)
    }
    
    func addComponents() {
        self.addSubview(alertContainer)
        alertContainer.addSubview(alertTitle)
        alertContainer.addSubview(acceptButton)
        alertContainer.addSubview(alertMessage)
        self.addSubview(cancelButton)
    }
    
    func setAutolayout() {
        largeConstraints = [
            alertContainer.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            alertContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40),
            alertContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40),
            
            alertTitle.topAnchor.constraint(equalTo: alertContainer.topAnchor, constant: 20),
            alertTitle.trailingAnchor.constraint(equalTo: alertContainer.trailingAnchor, constant: -20),
            alertTitle.leadingAnchor.constraint(equalTo: alertContainer.leadingAnchor, constant: 20),
            
            acceptButton.topAnchor.constraint(equalTo: alertTitle.bottomAnchor, constant: 20),
            acceptButton.centerXAnchor.constraint(equalTo: alertContainer.centerXAnchor),
            acceptButton.widthAnchor.constraint(equalToConstant: 60),
            acceptButton.heightAnchor.constraint(equalToConstant: 60),
            
            alertMessage.topAnchor.constraint(equalTo: acceptButton.bottomAnchor, constant: 20),
            alertMessage.trailingAnchor.constraint(equalTo: alertContainer.trailingAnchor, constant: -20),
            alertMessage.leadingAnchor.constraint(equalTo: alertContainer.leadingAnchor, constant: 20),
            
            cancelButton.topAnchor.constraint(equalTo: alertMessage.bottomAnchor, constant: 20),
            cancelButton.bottomAnchor.constraint(equalTo: alertContainer.bottomAnchor, constant: -10),
            cancelButton.trailingAnchor.constraint(equalTo: alertContainer.trailingAnchor, constant: -10),
            cancelButton.leadingAnchor.constraint(equalTo: alertContainer.leadingAnchor, constant: 10),
            cancelButton.heightAnchor.constraint(equalToConstant: 50)
        ]
    }
    
    func activateCurrentLayout() {
        NSLayoutConstraint.activate(largeConstraints)
    }
    
}

extension BiometricsAlertView {
    
    @objc private func didAcceptButtonPress() {
        delegate?.didAcceptButtonTap(self)
    }
    
    @objc private func didCancelButtonPress() {
        delegate?.didCancelButtonTap(self)
    }
}


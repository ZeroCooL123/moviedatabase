//
//  SingleAlertView.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import UIKit

class SingleAlertView: AlertView {
    
    var compactConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var regularConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var largeConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    
    let alertContainer: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .white
        view.layer.cornerRadius = 10.0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let iconContainer: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .white
        view.layer.cornerRadius = 50
        view.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        view.layer.shadowRadius = 3.0
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let alertIcon: UIImageView = {
        let imageView = UIImageView(frame: CGRect.zero)
        imageView.image = UIImage()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    let alertTitle: UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.text = ""
        label.textColor = .gray
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let alertMessage: UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.text = ""
        label.textColor = .gray
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    var cancelButton: UIButton = UIButton()
    
    var acceptButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.setTitle("Aceptar", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.main
        button.layer.cornerRadius = 10.0
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(didAcceptButtonPress), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initComponents()
    }
    
    required init(content: AlertContent?) {
        self.init()
        
        alertIcon.image = content?.icon
        alertTitle.text = content?.title
        alertMessage.text = content?.description
        acceptButton.setTitle(content?.acctionButtonTitle, for: .normal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initComponents() {
        self.addComponents()
        self.setAutolayout()
        self.activateCurrentLayout()
        self.setAcceptButton(acceptButton)
    }
    
    func addComponents() {
        self.addSubview(alertContainer)
        alertContainer.addSubview(iconContainer)
        iconContainer.addSubview(alertIcon)
        alertContainer.addSubview(alertTitle)
        alertContainer.addSubview(alertMessage)
        self.addSubview(acceptButton)
    }
    
    func setAutolayout() {
        largeConstraints = [
            alertContainer.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            alertContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40),
            alertContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40),
            
            iconContainer.centerYAnchor.constraint(equalTo: alertContainer.topAnchor),
            iconContainer.centerXAnchor.constraint(equalTo: alertContainer.centerXAnchor),
            iconContainer.widthAnchor.constraint(equalToConstant: 100),
            iconContainer.heightAnchor.constraint(equalToConstant: 100),
            
            alertIcon.centerYAnchor.constraint(equalTo: iconContainer.centerYAnchor),
            alertIcon.centerXAnchor.constraint(equalTo: iconContainer.centerXAnchor),
            alertIcon.widthAnchor.constraint(equalToConstant: 60),
            alertIcon.heightAnchor.constraint(equalToConstant: 60),
            
            alertTitle.topAnchor.constraint(equalTo: iconContainer.bottomAnchor, constant: 20),
            alertTitle.trailingAnchor.constraint(equalTo: alertContainer.trailingAnchor, constant: -20),
            alertTitle.leadingAnchor.constraint(equalTo: alertContainer.leadingAnchor, constant: 20),
            
            alertMessage.topAnchor.constraint(equalTo: alertTitle.bottomAnchor, constant: 10),
            alertMessage.trailingAnchor.constraint(equalTo: alertContainer.trailingAnchor, constant: -20),
            alertMessage.leadingAnchor.constraint(equalTo: alertContainer.leadingAnchor, constant: 20),
            
            acceptButton.topAnchor.constraint(equalTo: alertMessage.bottomAnchor, constant: 20),
            acceptButton.bottomAnchor.constraint(equalTo: alertContainer.bottomAnchor, constant: -10),
            acceptButton.trailingAnchor.constraint(equalTo: alertContainer.trailingAnchor, constant: -10),
            acceptButton.leadingAnchor.constraint(equalTo: alertContainer.leadingAnchor, constant: 10),
            acceptButton.heightAnchor.constraint(equalToConstant: 50)
        ]
    }
    
    func activateCurrentLayout() {
        NSLayoutConstraint.activate(largeConstraints)
    }

}

extension SingleAlertView {
    
    @objc private func didAcceptButtonPress() {
        delegate?.didAcceptButtonTap(self)
    }
}


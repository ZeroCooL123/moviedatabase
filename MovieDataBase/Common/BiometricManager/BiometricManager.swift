//
//  BiometricManager.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import LocalAuthentication

enum BiometricType {
    case none
    case touch
    case face
}

protocol BiometricManagerDelegate {
    func authenticate(completion: @escaping (_ success: Bool) -> Void)
    func getBiometricType(completion: @escaping (_ type: BiometricType) -> Void)
    func registerBiometrics(completion: @escaping (_ success: Bool) -> Void)
}

class BiometricManager: BiometricManagerDelegate {
    
    var localAuthenticationContext: LAContext

    init(localAuthenticationContext: LAContext = LAContext()) {
        self.localAuthenticationContext = localAuthenticationContext
    }
    
    func registerBiometrics(completion: @escaping (Bool) -> Void) {

        var error: NSError?
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {

            let reason = "Log in to your account"
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason ) { success, error in

                if success {

                    // Move to the main thread because a state update triggers UI changes.
                    DispatchQueue.main.async {
                        completion(success)
                    }

                } else {
                    completion(false)
                    print(error?.localizedDescription ?? "Failed to authenticate")

                    // Fall back to a asking for username and password.
                    // ...
                }
            }
        } else {
            completion(false)
            print(error?.localizedDescription ?? "Can't evaluate policy")

            // Fall back to a asking for username and password.
            // ...
        }
    }

    func authenticate(completion: @escaping (_ success: Bool) -> Void) {
        var error: NSError?
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, _ in
                DispatchQueue.main.async {
                    completion(success)
                }
            }
        } else {
            completion(false)
        }
        localAuthenticationContext = LAContext()
    }
    
    func getBiometricType(completion: @escaping (BiometricType) -> Void) {
        if #available(iOS 11, *) {
            _ = localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            switch localAuthenticationContext.biometryType {
            case .touchID:
                completion(.touch)
            case .faceID:
                completion(.face)
            default:
                completion(.none)
            }
        } else {
            completion(localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touch : .none)
        }
    }
    
}


//
//  Coordinator.swift
//  ArquitectureExample
//
//  Created by Jose Raúl on 26/02/21.
//  Copyright © 2021 JoseRaulCompany. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}

protocol ParentCoordinator: Coordinator {
    func childDidFinish(_ child: Coordinator?)
}

protocol ChildCoordinator: Coordinator {
    func finish(completion: (() -> Void)?)
}

extension Coordinator {
    
    func presentOnTopViewController(viewController: UIViewController, animated: Bool) {
        if let presentedViewController = navigationController.presentedViewController {
            if let presentedViewController = presentedViewController.presentedViewController {
                presentedViewController.present(viewController, animated: animated)
            } else {
                presentedViewController.present(viewController, animated: animated)
            }
        } else {
            navigationController.present(viewController, animated: animated)
        }
    }

    func dismissOnTopViewController(animated: Bool, completion: (() -> Void)? = nil) {
        if let presentedViewController = navigationController.presentedViewController {
            if let presentedViewController = presentedViewController.presentedViewController {
                presentedViewController.dismiss(animated: animated, completion: completion)
            } else {
                presentedViewController.dismiss(animated: animated, completion: completion)
            }
        } else {
            navigationController.dismiss(animated: animated, completion: completion)
        }
    }

    func pushViewOnNavigationTop(viewController: UIViewController, animated: Bool) {
        if let presentedViewController = navigationController.presentedViewController {
            presentedViewController.navigationController?.pushViewController(viewController, animated: false)
        } else {
            navigationController.pushViewController(viewController, animated: animated)
        }
    }
}


//
//  MainCoordinator.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

typealias Factories = AlertFactory&ExampleFactory&LoginFactory&HomeFactory&SelectedShowFactory

protocol MainCoordinatorProtocol: ParentCoordinator {
    func present(_ vc: UIViewController)
    func createAlert(content: AlertContent?, type: AlertType) -> AlertVC
    func hideLoader(_ completion: (() -> Void)?)
    func showLoader()
    func goLogin()
    func goHome()
    func goSelectedShow(name: String, poster: String, dateAir: String, overView: String, backDrop: String, vote: String, id: String)

}

class MainCoordinator: MainCoordinatorProtocol {

    
    func goSelectedShow(name: String, poster: String, dateAir: String, overView: String, backDrop: String, vote: String, id: String) {
        
        let view = SelectedShowView()
        view.backgroundColor = UIColor.black
        view.SelectedNamee = name
        view.SelectedPosterr = poster
        view.SelectedDateAirr = dateAir
        view.SelectedOverVieww = overView
        view.SelectedBackdropp = backDrop
        view.SelectedVotee = vote
        view.SelectedIDD = id
        let vc = factory.makeSelectedShowVC(view, coordinator: self)
        navigationController.pushViewController(vc, animated: false)
        
    }
    
    func goHome() {
        
        let view = HomeView()
        view.backgroundColor = UIColor.black
        let vc = factory.makeHomeVC(view, coordinator: self)
        navigationController.isNavigationBarHidden = false
        navigationController.setViewControllers([vc], animated: true)
    }
    
    func goLogin() {
        let view = LoginView()
        view.backgroundColor = UIColor.mainDark
        let vc = factory.makeLoginVC(view, coordinator: self)
        navigationController.pushViewController(vc, animated: false)
    }
    
    
    
    
    

    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    private let factory: Factories
    
    init(_ factory: Factories, navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.factory = factory
    }
    
    func present(_ vc: UIViewController) {
        navigationController.present(vc, animated: false, completion: nil)
    }
    
    func createAlert(content: AlertContent?, type: AlertType) -> AlertVC {
        let view = factory.makeAlertView()
        let vc = factory.makeAlertVC(view, coordinator: self, type: type, content: content)
        vc.title = ""
        
        return vc
    }
    
    func hideLoader(_ completion: (() -> Void)?) {
        self.navigationController.dismiss(animated: false) {
            completion?()
        }
    }
    
    func showLoader() {
        DispatchQueue.main.async {
            let view = LoaderView()
            let vc = LoaderVC(UI: view)
            vc.modalPresentationStyle = .overCurrentContext
            
            self.navigationController.present(vc, animated: false, completion: nil)
        }
    }
    
    func start() {
        let view = factory.makeExampleView(UIColor.white)
        let vc = factory.makeExampleVC(view, coordinator: self)
        vc.title = ""

        navigationController.pushViewController(vc, animated: false)
    }
    
    func childDidFinish(_ child: Coordinator?) {
        if let childCoordinator = child {
            childCoordinators = childCoordinators.filter { $0 !== childCoordinator }
        }
    }
}



//
//  HomeFactory.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 2/28/21.
//

import Foundation
import UIKit

protocol HomeFactory {
    func makeHomeViewModel(_ view: HomeViewProtocol, repository: HomeRepositoryProtocol) -> HomeViewModelProtocol
    func makeHomeRepository() -> HomeRepository
    func makeHomeView(_ bgColor: UIColor) -> HomeView
    func makeHomeVC(_ view: HomeView, coordinator: MainCoordinator) -> HomeVC
}

extension DependencyContainer: HomeFactory {
    

    
    func makeHomeViewModel(_ view: HomeViewProtocol, repository: HomeRepositoryProtocol) -> HomeViewModelProtocol {
        let viewModel = HomeViewModel(view: view, repository: repository, defaults: defaults)
        
        return viewModel
    }
    
    func makeHomeRepository() -> HomeRepository {
        return HomeRepository(networkService: networkService, localService: localService)
    }
    
    func makeHomeView(_ bgColor: UIColor) -> HomeView {
        let view = HomeView()
        view.backgroundColor = bgColor
        
        return view
    }
    
    func makeHomeVC(_ view: HomeView, coordinator: MainCoordinator) -> HomeVC {
        let vc = HomeVC(UI: view, factory: self, coordinator: coordinator, defaults: defaults)
        
        return vc
    }

}

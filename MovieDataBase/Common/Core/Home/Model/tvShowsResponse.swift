//
//  tvShowsResponse.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 2/28/21.
//

import Foundation
/*
 {
     "status_code": 7,
     "status_message": "Invalid API key: You must be granted a valid key.",
     "success": false
 }
 */
struct TvShowsResponseStruct: Initiable {

    let success: Bool?
    let status_message: String?
    let status_code: String?
    let page: Int?
    let results: [TvShowsStruct]?

    
    init() {
      
    
        self.success = false
        self.status_code = ""
        self.status_message = ""
        self.page = 0
        self.results = [TvShowsStruct]()
    }
}




struct TvShowsStruct: Initiable {
    
    let poster_path: String?
    let name: String?
    let first_air_date: String?
    let vote_average: Double?
    let overview: String?
    let id: Int?
    let backdrop_path: String?

    init() {
        
        self.poster_path = ""
        self.name = ""
        self.first_air_date = ""
        self.vote_average = 0.0
        self.overview = ""
        self.id = 0
        self.backdrop_path = ""

        }
    
    init(dictionary: NSDictionary) {
        self.poster_path = dictionary["poster_path"] as? String ?? ""
        self.name = dictionary["name"] as? String ?? ""
        self.first_air_date = dictionary["first_air_date"] as? String ?? ""
        self.vote_average = dictionary["vote_average"] as? Double ?? 0.0
        self.overview = dictionary["overview"] as? String ?? ""
        self.backdrop_path = dictionary["backdrop_path"] as? String ?? ""
        self.id = dictionary["id"] as? Int ?? 0
    }
    
}


struct imageStruct: Initiable {
    private var image: [String] = [""]
    
    init() {
        
    }
      
    init(image: [String]) {

        self.image = image
    }
}
/*
 "page": 1,
    "results": [
        {
            "backdrop_path": "/lOr9NKxh4vMweufMOUDJjJhCRHW.jpg",
            "first_air_date": "2021-01-15",
            "genre_ids": [
                10765,
                9648,
                18
            ],
            "id": 85271,
            "name": "WandaVision",
            "origin_country": [
                "US"
            ],
            "original_language": "en",
            "original_name": "WandaVision",
            "overview": "Wanda Maximoff and Vision—two super-powered beings living idealized suburban lives—begin to suspect that everything is not as it seems.",
            "popularity": 5511.607,
            "poster_path": "/frobUz2X5Pc8OiVZU8Oo5K3NKMM.jpg",
            "vote_average": 8.5,
            "vote_count": 6134
        },
 */

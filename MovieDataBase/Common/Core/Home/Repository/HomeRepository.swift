//
//  HomeRepository.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 2/28/21.
//

import Foundation

protocol HomeRepositoryProtocol: WebRepository {
    func callRequestForTVShows(_ parameters: tvShowsRequest, completion: @escaping (TvShowsResponseStruct?, NetworkError?) -> Void)
}

class HomeRepository: HomeRepositoryProtocol {
    func callRequestForTVShows(_ parameters: tvShowsRequest, completion: @escaping (TvShowsResponseStruct?, NetworkError?) -> Void) {
        
        guard let addAccountURL = URL(string: "https://api.themoviedb.org/3/tv/popular?api_key=8d9ced07152c868de3f180fef43510a7&language=en-US&page=1") else {
            fatalError("URL was incorrect")
        }
        
        let params = AnyEncodable(value: parameters)
        
        let resource = Resource<TvShowsResponseStruct>(url: addAccountURL, parameters: params, authorizationKey: "")
        
        networkService.getRequest(resource: resource) { (result) in
            print(result)
            switch result {
            case .success(let response):
                completion(response, nil)
            case .failure(let error):
                switch error {
                case .decodingError:
                    print(NetworkError.decodingError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.decodingError.rawValue))
                case .domainError:
                    print(NetworkError.domainError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.domainError.rawValue))
                    
                case .timeoutError:
                    print(NetworkError.timeoutError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.timeoutError.rawValue))
                default:
                    print(NetworkError.urlError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.decodingError.rawValue))
                }
            }
        }
    }
    
    
    
    

    var localService: LocalManager
    var networkService: NetworkManager
    
    
    
    required init(networkService: NetworkManager, localService: LocalManager) {
        self.networkService = networkService
        self.localService = localService
    }

}


//
//  TvShowsCell.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 2/28/21.
//

import UIKit

class TvShowsCell: UICollectionViewCell {
    var model: TvShowsStruct? {
        didSet {
            guard let viewModel = model else { return }
            let guardado = viewModel.poster_path
            tvTitleLabel.text = viewModel.name
            tvDateLabel.text = viewModel.first_air_date
            let multiConvertido = String(format: "%.1f", viewModel.vote_average ?? 0.0)
            tvRatingLabel.text = multiConvertido
            tvDescriptionLabel.text = viewModel.overview

            let image = "https://image.tmdb.org/t/p/w500\(guardado ?? "")"
            if image == "" || image == "undefined" {
                imageView.image = UIImage(named: "")
                imageView.contentMode = .scaleAspectFill
            }else{
                imageView.downloaded(from: image)
                imageView.contentMode = .scaleAspectFill
            }
        }
    }
    
    
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        
        let modelName = UIDevice().type
        
        if modelName.rawValue == "iPhone 7" || modelName.rawValue == "iPhone 8" || modelName.rawValue == "iPhone 6" || modelName.rawValue == "iPhone 6s" || modelName.rawValue == "iPhone 8 Plus" || modelName.rawValue == "iPhone 7 Plus" || modelName.rawValue == "iPhone 6s Plus" || modelName.rawValue == "iPhone SE 2nd gen"{
            tvRatingLabel.adjustsFontSizeToFitWidth = true
            tvDateLabel.adjustsFontSizeToFitWidth = true
        }else{
            
        }
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .clear
        iv.alpha = 1
        iv.image = UIImage()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    var tvTitleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.font = Fonts.AVENIRBold.of(size: 14)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = UIColor.algaeGreen
        return label
    
        }()
    
    var tvDateLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.font = Fonts.AVENIR.of(size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = UIColor.algaeGreen
        return label
    
        }()
    
    let starIcon: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .clear
        iv.image = UIImage(named: "star")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    var tvRatingLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.font = Fonts.AVENIR.of(size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.textColor = UIColor.algaeGreen
        return label
    
        }()
    
    var tvDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 0
        label.font = Fonts.AVENIR.of(size: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .justified
        label.textColor = UIColor.white
        return label
    
        }()
    

    
    func setupViews() {
        addSubview(imageView)
        addSubview(tvTitleLabel)
        addSubview(tvDateLabel)
        addSubview(starIcon)
        addSubview(tvRatingLabel)
        addSubview(tvDescriptionLabel)
        
        
        NSLayoutConstraint.activate([

            imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            imageView.topAnchor.constraint(equalTo: self.topAnchor),
            imageView.widthAnchor.constraint(equalTo: self.widthAnchor),
            imageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.65),
            
            tvTitleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12),
            tvTitleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 6),
            tvTitleLabel.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.05),
            tvTitleLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8),
            
            tvDateLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12),
            tvDateLabel.topAnchor.constraint(equalTo: tvTitleLabel.bottomAnchor, constant: 6),
            tvDateLabel.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.05),
            tvDateLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            
            
            starIcon.centerYAnchor.constraint(equalTo: tvRatingLabel.centerYAnchor),
            starIcon.rightAnchor.constraint(equalTo: tvRatingLabel.leftAnchor, constant: -6),
            starIcon.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.08),
            starIcon.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.08),
            
            tvRatingLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12),
            tvRatingLabel.topAnchor.constraint(equalTo: tvTitleLabel.bottomAnchor, constant: 6),
            tvRatingLabel.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.05),
            tvRatingLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1),
            
            tvDescriptionLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            tvDescriptionLabel.topAnchor.constraint(equalTo: tvDateLabel.bottomAnchor, constant: 6),
            tvDescriptionLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -2),
            tvDescriptionLabel.leftAnchor.constraint(equalTo: tvDateLabel.leftAnchor),
            tvDescriptionLabel.rightAnchor.constraint(equalTo: tvRatingLabel.rightAnchor)
            
        ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

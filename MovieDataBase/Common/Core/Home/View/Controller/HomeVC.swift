//
//  HomeVC.swift
//  ArquitectureHome
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    // MARK: - Controller's properties

    let UI: HomeView
    let coordinator: MainCoordinator
    let defaults: DefaultsManager
    private var factory: HomeFactory
    lazy var viewModel: HomeViewModelProtocol = {
        factory.makeHomeViewModel(self, repository: factory.makeHomeRepository())
    }()

   
    
    // MARK: - Controller's initializers

    required init(UI: HomeView, factory: HomeFactory, coordinator: MainCoordinator, defaults: DefaultsManager) {
        self.UI = UI
        self.factory = factory
        self.coordinator = coordinator
        self.defaults = defaults

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK: - Controller's life cycle

    override func loadView() {
        self.view = self.UI
        UI.delegate = self
        UI.tvCollectionView.delegate = self
        UI.tvCollectionView.dataSource = self
        UI.secmentedControllerR.addTarget(self, action: #selector(segmentedChange(sender:)), for: .valueChanged)
    }

    override func viewDidLayoutSubviews() {
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = UIColor.mainDark
        self.navigationItem.title = "TV Shows"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        UI.secmentedControllerR.selectedSegmentIndex = 0
        DispatchQueue.main.async { [self] in
            if InternetConnectionManager.isConnectedToNetwork() == true{
                getTVShows(url: "https://api.themoviedb.org/3/tv/popular?api_key=8d9ced07152c868de3f180fef43510a7&language=en-US&page=1")
            }
            else{
                self.view.makeToast(message: "No internet connection")
            }
        
        }
        
    }
    @objc func segmentedChange(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            
            UI.SelectedBackdrop = ""
            arrayShows.removeAll()
            DispatchQueue.main.async { [self] in

                if InternetConnectionManager.isConnectedToNetwork() == true{
                    getTVShows(url: "https://api.themoviedb.org/3/tv/popular?api_key=8d9ced07152c868de3f180fef43510a7&language=en-US&page=1")
                }
                else{
                    self.view.makeToast(message: "No internet connection")
                }
            }
            
        case 1:
            
            UI.SelectedBackdrop = ""
            arrayShows.removeAll()
            DispatchQueue.main.async { [self] in
            
                if InternetConnectionManager.isConnectedToNetwork() == true{
                    getTVShows(url: "https://api.themoviedb.org/3/tv/top_rated?api_key=8d9ced07152c868de3f180fef43510a7&language=en-US&page=1")
                }
                else{
                    self.view.makeToast(message: "No internet connection")
                }
            }
            
        case 2:
            
            UI.SelectedBackdrop = ""
            arrayShows.removeAll()
            DispatchQueue.main.async { [self] in
            
                
                if InternetConnectionManager.isConnectedToNetwork() == true{
                    getTVShows(url: "https://api.themoviedb.org/3/tv/on_the_air?api_key=8d9ced07152c868de3f180fef43510a7&language=en-US&page=1")
                }
                else{
                    self.view.makeToast(message: "No internet connection")
                }
            }
            
        case 3:
            
            UI.SelectedBackdrop = ""
            arrayShows.removeAll()
            DispatchQueue.main.async { [self] in
            
                
                if InternetConnectionManager.isConnectedToNetwork() == true{
                    getTVShows(url: "https://api.themoviedb.org/3/tv/airing_today?api_key=8d9ced07152c868de3f180fef43510a7&language=en-US&page=1")
                }
                else{
                    self.view.makeToast(message: "No internet connection")
                }
            }
            
        default:
            print("none")
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        UI.SelectedBackdrop = ""
    }

    override func viewDidAppear(_ animated: Bool) {}

    override func viewWillDisappear(_ animated: Bool) {}

    override func viewDidDisappear(_ animated: Bool) {}

    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }
    


}

extension HomeVC: HomeViewProtocol {
    func getDataFromTVShows(data: TvShowsResponseStruct) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
        let arrayReponse = data.results
        
           
        }
        
    }
    

    
    
}

extension HomeVC: HomeViewDelegate {
    func popularBTN(_ view: HomeView) {
        
    }
    
    func topRatedBTN(_ view: HomeView) {
        
    }
    
    func onTVBTN(_ view: HomeView) {
        
    }
    
    func airingTodayBTN(_ view: HomeView) {
        
    }
    
    

}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayShows.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "identifier", for: indexPath) as! TvShowsCell
    
        cell.backgroundColor = .mainDark
        cell.model = arrayShows[indexPath.item]
        cell.layer.cornerRadius = 10
        return cell
    
}
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let modelName = UIDevice().type
        
        if modelName.rawValue == "iPhone 7" || modelName.rawValue == "iPhone 8" || modelName.rawValue == "iPhone 6" || modelName.rawValue == "iPhone 6s" || modelName.rawValue == "iPhone 8 Plus" || modelName.rawValue == "iPhone 7 Plus" || modelName.rawValue == "iPhone 6s Plus" || modelName.rawValue == "iPhone SE 2nd gen"{
            return CGSize(width: 140, height: 300)
        }
        else{
            return CGSize(width: 180, height: 400)
        }
       
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
       
           return 10
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "identifier", for: indexPath) as! TvShowsCell
    
        if InternetConnectionManager.isConnectedToNetwork() == true{
            cell.model = arrayShows[indexPath.item]
            let nombre = arrayShows[indexPath.row].name ?? ""
            let poster = arrayShows[indexPath.row].poster_path ?? ""
            let date = arrayShows[indexPath.row].first_air_date ?? ""
            let overview = arrayShows[indexPath.row].overview ?? ""
            let backdrop = arrayShows[indexPath.row].backdrop_path ?? ""
            let voteAverage = arrayShows[indexPath.row].vote_average ?? 0.0
            let id = arrayShows[indexPath.row].id ?? 0
      
            coordinator.goSelectedShow(name: nombre, poster: poster, dateAir: date, overView: overview, backDrop: backdrop, vote: "\(voteAverage)", id: "\(id)")
            
            arraySeasonsEpisodes.removeAll()
        }
        else{
            self.view.makeToast(message: "No internet connection")
        }
        
        
    }
    
    
    
}

extension HomeVC{
    func getTVShows(url: String){
        var semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          semaphore.signal()
            do {
                 let json = try JSONSerialization.jsonObject(with: data)
                 if let jsonArray = json as? [[String:Any]] {
                    
                 } else if let jsonDictionary = json as? NSDictionary {
                    
                    var toneCategories = jsonDictionary["results"] as? [NSDictionary] ?? []
                    
                    arrayShows.removeAll()
                       for category in toneCategories {
                        let nombre = category["name"]
                        arrayName.append(nombre as? String ?? "")
                        
                        let imagen = category["poster_path"]
                        arrayPoster_path.append(imagen as? String ?? "")
                        
                        
                        let fecha = category["first_air_date"]
                        arrayFirst_air_date.append(fecha as? String ?? "")
                        
                        let voto = category["vote_average"]
                        arrayVote_average.append(voto as? NSNumber ?? 0)

                        
                        let descrp = category["overview"]
                        arrayOverview.append(descrp as? String ?? "")
                        
                        let poster = category["backdrop_path"]
                        arrayBackdrop_path.append(poster as? String ?? "")
                        
                        let id = category["id"]
                        arrayid.append(id as? NSNumber ?? 0)
                        
                        let show = TvShowsStruct(dictionary: category)
                        arrayShows.append(show)
                       
                        
                       }
                    DispatchQueue.main.async {
                        self.UI.tvCollectionView.reloadData()
                    }
                   
                    
                 } else {
                    print("This should never be displayed")
                 }
            }
            catch let error as NSError {
                
                print(error.localizedDescription)
            }
            
        }

        task.resume()
        semaphore.wait()
    }
}

//
//  HomeView.swift
//  ArquitectureHome
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

var imagenArraaay: [String] = []
var arrayPoster_path: [String] = []
var arrayOverview: [String] = []
var arrayCreatorName: [String] = []
var season_number: String = ""
var season_number2: Int = 0
var air_date: String = ""
var still_path: String = ""




protocol HomeViewDelegate: NSObjectProtocol {
    func popularBTN(_ view: HomeView)
    func topRatedBTN(_ view: HomeView)
    func onTVBTN(_ view: HomeView)
    func airingTodayBTN(_ view: HomeView)
   
    
}

enum HomeStrings: String {
    case Home = "Username"
    case password = "Password"
    case button = "Log in"
}

class HomeView: UIView, ViewDelegate {


    var SelectedName: String = ""
    var SelectedPoster: String = ""
    var SelectedDateAir: String = ""
    var SelectedOverView: String = ""
    var SelectedBackdrop: String = ""
    var SelectedVote: String = ""
    var SelectedID: String = ""

    weak var delegate: HomeViewDelegate?

    
    var compactConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var regularConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var largeConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()

    
    
    
    let secmentedControllerR: UISegmentedControl = {
        let segmentItems = ["Popular", "Top Rated", "On TV", "Airing Today"]
        let font = UIFont.systemFont(ofSize: 10)
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        let layout = UISegmentedControl(items: segmentItems)
        layout.backgroundColor = .mainDark
        
        layout.translatesAutoresizingMaskIntoConstraints = false
        return layout
    }()
    
    
    
    
    lazy var tvCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 10
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.clear
        cv.register(TvShowsCell.self, forCellWithReuseIdentifier: "identifier")
        cv.isPagingEnabled = true
     cv.showsVerticalScrollIndicator = false
        
     cv.showsHorizontalScrollIndicator = false
     cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    


    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        initComponents()
    }

    func initComponents() {
        addComponents()
        setAutolayout()
        activateCurrentLayout()
        setDelegates()
        addObservers()
        
        

    }

    func addComponents() {

       addSubview(secmentedControllerR)
       addSubview(tvCollectionView)
        
        
    }
    
    func setDelegates() {
        
    }
    
    

    func setAutolayout() {
        // Compact Constraints
        compactConstraints = [
            secmentedControllerR.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            secmentedControllerR.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 30),
            secmentedControllerR.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.04),
            secmentedControllerR.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8),
            
            tvCollectionView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            tvCollectionView.topAnchor.constraint(equalTo: secmentedControllerR.bottomAnchor, constant: 8),
            tvCollectionView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            tvCollectionView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
            
        ]

        // Regular Constraints
        regularConstraints = [
            secmentedControllerR.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            secmentedControllerR.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 30),
            secmentedControllerR.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.04),
            secmentedControllerR.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8),
            
            tvCollectionView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            tvCollectionView.topAnchor.constraint(equalTo: secmentedControllerR.bottomAnchor, constant: 8),
            tvCollectionView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            tvCollectionView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
            
        ]

        // Large Constraints
        largeConstraints = [
            
           
            secmentedControllerR.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            secmentedControllerR.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 30),
            secmentedControllerR.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.04),
            secmentedControllerR.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8),
            
            tvCollectionView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            tvCollectionView.topAnchor.constraint(equalTo: secmentedControllerR.bottomAnchor, constant: 8),
            tvCollectionView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            tvCollectionView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
            

        ]
    }

    func activateCurrentLayout() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                // iPhone 5 or 5S or 5C
                NSLayoutConstraint.activate(compactConstraints)
            case 1334:
                // iPhone 6/6S/7/8
                NSLayoutConstraint.activate(regularConstraints)

            case 2208:
                // iPhone 6+/6s+/7+/8+
                NSLayoutConstraint.activate(regularConstraints)
            case 2436:
                // iPhone X/Xs/11 Pro
                NSLayoutConstraint.activate(largeConstraints)
            case 1792:
                // iPhone 11/XR
                NSLayoutConstraint.activate(largeConstraints)
            default:
                // 11 Pro Max/Xs Max
                NSLayoutConstraint.activate(largeConstraints)
            }
        } else if UIDevice().userInterfaceIdiom == .pad {
            // iPad cases
            NSLayoutConstraint.activate(regularConstraints)
        }
    }

    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(didKeyboardShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didKeyboardShow), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
        
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
        
    @objc
    func didKeyboardShow(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = self.convert(keyboardScreenEndFrame, from: self.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            isKeyboardOpen(true, keyboardFrame: keyboardViewEndFrame)
        } else {
            isKeyboardOpen(false, keyboardFrame: keyboardViewEndFrame)
        }
    }
        
    func isKeyboardOpen(_ isOpen: Bool, keyboardFrame: CGRect) {
        
        if isOpen {
           
        } else {
            
            let modelName = UIDevice.current.modelName
            
            if modelName == "iPhone 7" || modelName == "iPhone 8" || modelName == "iPhone 6" || modelName == "iPhone 6s" || modelName == "iPhone 8 Plus" || modelName == "iPhone 7 Plus" || modelName == "iPhone 6s Plus"{
              
            }
            else{
               
            }
        }
    }
        
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }

}

extension HomeView {
    @objc private func popular() {
        delegate?.popularBTN(self)
    }
    @objc private func topRated() {
        delegate?.topRatedBTN(self)
    }
    @objc private func onTV() {
        delegate?.onTVBTN(self)
    }
    @objc private func airingToday() {
        delegate?.airingTodayBTN(self)
    }


}



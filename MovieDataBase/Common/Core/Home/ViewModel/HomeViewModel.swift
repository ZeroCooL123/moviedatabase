//
//  HomeViewModel.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 2/28/21.
//

import Foundation

var arrayName: [String] = []
var arrayFirst_air_date: [String] = []
var arrayBackdrop_path: [String] = []
var arrayVote_average: [NSNumber] = []
var arrayid: [NSNumber] = []
var arrayShows: [TvShowsStruct] = []
var arrayCast: [CastStruct] = []
var arrayCreator: [CreatedStruct] = []
var arraySeasonsEpisodes: [EpisodesStruct] = []
var arraySeasonsEpisodes2: [EpisodesStruct] = []
var arraySeasonsEpisodes3: [EpisodesStruct] = []
var arraySeasonsEpisodes4: [EpisodesStruct] = []
var arraySeasonsEpisodes5: [EpisodesStruct] = []
var arraySeasonsEpisodes6: [EpisodesStruct] = []
var arraySeasonsEpisodes7: [EpisodesStruct] = []
var arraySeasonsEpisodes8: [EpisodesStruct] = []
var arraySeasonsEpisodes9: [EpisodesStruct] = []
var arraySeasonsEpisodes10: [EpisodesStruct] = []
var arregloDeArreglos: [[ArregloBidimencionalStruct]] = []
var arregloDeArreglosTri: [ArregloTridimencionalStruct] = []
var arrayOfSeasonsInt: [Int] = []


protocol HomeViewProtocol {
    func getDataFromTVShows(data: TvShowsResponseStruct)
}

protocol HomeViewModelProtocol {
    func callDataFromTVShows(request: tvShowsRequest)
    
}

class HomeViewModel: HomeViewModelProtocol {
    func callDataFromTVShows(request: tvShowsRequest) {
        repository.callRequestForTVShows(request) { (response, error) in
            if error?.localizedDescription == "The Internet connection appears to be offline."{
                print("no hay internet")
                
            }
            if error == NetworkError.timeoutError {
                print("esperó 30s")
               

            }
            
            if error == NetworkError.nointernet {
                print("no hay internet")
                
            }
            
            if error == NetworkError.sinSalidaNetwork {
                print("no hay internet")
                
            }

            
            if let response = response {
                print("response")
                self.view.getDataFromTVShows(data: response)
            }
            
        }
    }
    
    
    
   
    
    var view: HomeViewProtocol
    var repository: HomeRepositoryProtocol
    var defaults: DefaultsManager
    
    init(view: HomeViewProtocol, repository: HomeRepositoryProtocol, defaults: DefaultsManager) {
        self.view = view
        self.repository = repository
        self.defaults = defaults
    }

    
}


//
//  LoginFactory.swift
//  ArquitectureLogin
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

protocol LoginFactory {
    func makeLoginViewModel(_ view: LoginViewProtocol, repository: LoginRepositoryProtocol) -> LoginViewModelProtocol
    func makeLoginRepository() -> LoginRepository
    func makeLoginView(_ bgColor: UIColor) -> LoginView
    func makeLoginVC(_ view: LoginView, coordinator: MainCoordinator) -> LoginVC
}

extension DependencyContainer: LoginFactory {
    

    
    func makeLoginViewModel(_ view: LoginViewProtocol, repository: LoginRepositoryProtocol) -> LoginViewModelProtocol {
        let viewModel = LoginViewModel(view: view, repository: repository, defaults: defaults)
        
        return viewModel
    }
    
    func makeLoginRepository() -> LoginRepository {
        return LoginRepository(networkService: networkService, localService: localService)
    }
    
    func makeLoginView(_ bgColor: UIColor) -> LoginView {
        let view = LoginView()
        view.backgroundColor = bgColor
        
        return view
    }
    
    func makeLoginVC(_ view: LoginView, coordinator: MainCoordinator) -> LoginVC {
        let vc = LoginVC(UI: view, factory: self, coordinator: coordinator, defaults: defaults)
        
        return vc
    }

}


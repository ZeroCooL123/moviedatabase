//
//  LoginRequest.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 2/28/21.
//

import Foundation

struct LoginRequest: Initiable {

    private var username: String = ""
    private var password: String = ""
    private var request_token: String = ""
    
    init() {
        
    }
      
    init(username: String, password: String, request_token: String) {

        self.username = username
        self.password = password
        self.request_token = request_token
        
        
    }
}


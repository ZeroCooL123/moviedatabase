//
//  LoginResponse.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 2/28/21.
//

import Foundation


struct LoginResponse: Initiable {

    let success: Bool?
    let expires_at: String?
    let request_token: String?
    let status_code: Int?
    let status_message: String?
    
    

    
    init() {
      
    
        self.success = false
        self.expires_at = ""
        self.request_token = ""
        self.status_code = 0
        self.status_message = ""
        
    }
}


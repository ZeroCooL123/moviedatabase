//
//  LoginRepository.swift
//  ArquitectureLogin
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

protocol LoginRepositoryProtocol: WebRepository {
    func callServiceToken(_ parameters: requestForTokenGuessStruct, completion: @escaping (ScheduleResponse?, NetworkError?) -> Void)
    func callServiceLogin(_ parameters: LoginRequest, completion: @escaping (LoginResponse?, NetworkError?) -> Void)
}

class LoginRepository: LoginRepositoryProtocol {
    func callServiceLogin(_ parameters: LoginRequest, completion: @escaping (LoginResponse?, NetworkError?) -> Void) {
        
        guard let addAccountURL = URL(string: "https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=8d9ced07152c868de3f180fef43510a7") else {
            fatalError("URL was incorrect")
        }
        
        let params = AnyEncodable(value: parameters)
        
        let resource = Resource<LoginResponse>(url: addAccountURL, parameters: params, authorizationKey: "")
        
        networkService.postRequest(resource: resource) { (result) in
            
            switch result {
            case .success(let response):
                completion(response, nil)
            case .failure(let error):
                switch error {
                case .decodingError:
                    print(NetworkError.decodingError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.decodingError.rawValue))
                case .domainError:
                    print(NetworkError.domainError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.domainError.rawValue))
                    
                case .timeoutError:
                    print(NetworkError.timeoutError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.timeoutError.rawValue))
                default:
                    print(NetworkError.urlError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.decodingError.rawValue))
                }
            }
        }
    }
    
    func callServiceToken(_ parameters: requestForTokenGuessStruct, completion: @escaping (ScheduleResponse?, NetworkError?) -> Void) {
        
        guard let addAccountURL = URL(string: "https://api.themoviedb.org/3/authentication/token/new?api_key=8d9ced07152c868de3f180fef43510a7") else {
            fatalError("URL was incorrect")
        }
        
        let params = AnyEncodable(value: parameters)
        
        let resource = Resource<ScheduleResponse>(url: addAccountURL, parameters: params, authorizationKey: "UserSingleton.shared.tokenAcess")
        
        networkService.getRequest(resource: resource) { (result) in
            print(result)
            switch result {
            case .success(let response):
                completion(response, nil)
            case .failure(let error):
                switch error {
                case .decodingError:
                    print(NetworkError.decodingError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.decodingError.rawValue))
                case .domainError:
                    print(NetworkError.domainError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.domainError.rawValue))
                    
                case .timeoutError:
                    print(NetworkError.timeoutError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.timeoutError.rawValue))
                default:
                    print(NetworkError.urlError.rawValue)
                    completion(nil, NetworkError(rawValue: NetworkError.decodingError.rawValue))
                }
            }
        }
    }
    

    var localService: LocalManager
    var networkService: NetworkManager
    
    
    
    required init(networkService: NetworkManager, localService: LocalManager) {
        self.networkService = networkService
        self.localService = localService
    }

}


//
//  LoginSingletons.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 2/28/21.
//

import Foundation
class LoginSingletons {

    
    static let shared = LoginSingletons()
    var requestToken: String = ""
    
    
    public static func getInstance() -> LoginSingletons { return shared }
    
}

//
//  LoginVC.swift
//  ArquitectureLogin
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    // MARK: - Controller's properties

    let UI: LoginView
    let coordinator: MainCoordinator
    let defaults: DefaultsManager
    private var factory: LoginFactory
    lazy var viewModel: LoginViewModelProtocol = {
        factory.makeLoginViewModel(self, repository: factory.makeLoginRepository())
    }()

    // MARK: - Controller's initializers

    required init(UI: LoginView, factory: LoginFactory, coordinator: MainCoordinator, defaults: DefaultsManager) {
        self.UI = UI
        self.factory = factory
        self.coordinator = coordinator
        self.defaults = defaults

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK: - Controller's life cycle

    override func loadView() {
        self.view = self.UI
        UI.delegate = self

    }

    override func viewDidLayoutSubviews() {}

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true

        
//        UI.emailTextField.text = "zerocool123"
//        UI.passwordTextField.text = "hola123"
//        UI.iniciarSesionButton.backgroundColor = .algaeGreen
//        UI.iniciarSesionButton.isEnabled = true
        
        // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(_ animated: Bool) {}

    override func viewDidAppear(_ animated: Bool) {}

    override func viewWillDisappear(_ animated: Bool) {}

    override func viewDidDisappear(_ animated: Bool) {}

    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }


}

extension LoginVC: LoginViewProtocol {
    func getDataFromLogin(data: LoginResponse) {
        
        coordinator.hideLoader(nil)
        if data.success == true{
            let token = data.request_token ?? ""
            LoginSingletons.shared.requestToken = token
            coordinator.goHome()
        }
        else{
            self.view.makeToast(message: "Error try again")
        }
    }
    
    func getDataFromToken(data: ScheduleResponse) {
        
        if data.success == true{
            let user = UI.emailTextField.text ?? ""
            let pass = UI.passwordTextField.text ?? ""
            let token = data.request_token ?? ""
            LoginSingletons.shared.requestToken = token
            let request = LoginRequest(username: user, password: pass, request_token: token)
            viewModel.callLoginService(request: request)
        }
        else{
            coordinator.hideLoader(nil)
            self.view.makeToast(message: "Error try again")
        }
    }
}

extension LoginVC: LoginViewDelegate {
    func typeEmail(_ view: LoginView) {
        if (UI.passwordTextField.text!.isEmpty)
            {
            UI.iniciarSesionButton.backgroundColor = .lightGray
            UI.iniciarSesionButton.isEnabled = false
                return
            }
            else if (UI.emailTextField.text!.isEmpty)
            {
                UI.iniciarSesionButton.backgroundColor = .lightGray
                UI.iniciarSesionButton.isEnabled = false
                return
            }
            else
            {
                UI.iniciarSesionButton.backgroundColor = .algaeGreen
                UI.iniciarSesionButton.isEnabled = true
            }
    }
    
    func typePassword(_ view: LoginView) {
        if (UI.passwordTextField.text!.isEmpty)
            {
            UI.iniciarSesionButton.backgroundColor = .lightGray
            UI.iniciarSesionButton.isEnabled = false
                return
            }
            else if (UI.emailTextField.text!.isEmpty)
            {
                UI.iniciarSesionButton.backgroundColor = .lightGray
                UI.iniciarSesionButton.isEnabled = false
                return
            }
            else
            {
                UI.iniciarSesionButton.backgroundColor = .algaeGreen
                UI.iniciarSesionButton.isEnabled = true
            }
    }
    
    func goHome(_ view: LoginView) {
        if UI.emailTextField.text?.isEmpty == true{
            self.view.makeToast(message: "You must a valid username")
        }
        else if UI.passwordTextField.text?.isEmpty == true{
            self.view.makeToast(message: "You must enter a valid password")
        }
        else{
            if InternetConnectionManager.isConnectedToNetwork() == true{
                let request = requestForTokenGuessStruct()
                viewModel.callTokenService(request: request)
                coordinator.showLoader()
            }
            else{
                self.view.makeToast(message: "No internet connection")
            }
            
        }
    }

}


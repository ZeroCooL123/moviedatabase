//
//  LoginView.swift
//  ArquitectureLogin
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewDelegate: NSObjectProtocol {
    func goHome(_ view: LoginView)
   func typeEmail(_ view: LoginView)
    func typePassword(_ view: LoginView)
}

enum loginStrings: String {
    case login = "Username"
    case password = "Password"
    case button = "Log in"
}

class LoginView: UIView, ViewDelegate {



    weak var delegate: LoginViewDelegate?

    var compactConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var regularConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var largeConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()

    
    let topImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = .clear
        iv.alpha = 1
        iv.image = UIImage()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let midImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = .clear
        iv.alpha = 1
        iv.image = UIImage(named: "theMovieDBLogo")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    
    
    var emailTextField: BPTextField = {
        let textfield = BPTextField()
        textfield.autocapitalizationType = .none
        textfield.keyboardType = UIKeyboardType.emailAddress
        textfield.textColor = UIColor.mainDark
        textfield.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : UIFont(name: "Helvetica-bold", size: 15) ?? 15])
        textfield.textAlignment = .left
        textfield.font = Fonts.AVENIR.of(size: 16)
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.addToolbar()
        textfield.layer.backgroundColor = UIColor.white.cgColor
        textfield.layer.cornerRadius = 6
        textfield.layer.borderWidth = 0.01
        textfield.layer.borderColor = UIColor.clear.cgColor
        textfield.layer.shadowOffset = CGSize(width: 0, height: 1)
        textfield.layer.shadowColor = UIColor.lightGray.cgColor
        textfield.layer.shadowOpacity = 1
        textfield.layer.shadowRadius = 3.5
        textfield.layer.masksToBounds = false
        textfield.textContentType = UITextContentType.oneTimeCode
        return textfield
          
        }()
    
    var passwordTextField: BPTextField = {
        let textfield = BPTextField()
        textfield.isSecureTextEntry = true
        textfield.keyboardType = UIKeyboardType.default
        textfield.textColor = UIColor.mainDark
        textfield.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : UIFont(name: "Helvetica-bold", size: 15) ?? 15])
        textfield.textAlignment = .left
        textfield.font = Fonts.AVENIR.of(size: 16)
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.addToolbar()
        textfield.setIcon()
        textfield.layer.backgroundColor = UIColor.white.cgColor
        textfield.layer.cornerRadius = 6
        textfield.layer.borderWidth = 0.01
        textfield.layer.borderColor = UIColor.clear.cgColor
        textfield.layer.shadowOffset = CGSize(width: 0, height: 1)
        textfield.layer.shadowColor = UIColor.lightGray.cgColor
        textfield.layer.shadowOpacity = 1
        textfield.layer.shadowRadius = 3.5
        textfield.layer.masksToBounds = false
        textfield.textContentType = UITextContentType.oneTimeCode
        return textfield
          
        }()
    
    
    
    var iniciarSesionButton : UIButton = {
       
      let textTitle = UIButton()
       let button = UIButton(frame: CGRect.zero)
        button.setTitle("Log in", for: UIControl.State.normal)
        button.titleLabel?.font = Fonts.AVENIRBold.of(size: 16)
        button.setTitleColor(UIColor.white, for: UIControl.State.normal)
        button.backgroundColor = .lightGray
        button.isEnabled = false
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.clear.cgColor
        button.layer.cornerRadius = 6
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(goHomeBtn), for: .touchUpInside)
        button.layer.cornerRadius = 6
        button.layer.borderWidth = 0.01
        button.layer.borderColor = UIColor.clear.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 1)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.layer.shadowOpacity = 1
        button.layer.shadowRadius = 3.5
        button.layer.masksToBounds = false
        return button
    }()
    
    let stackViewForButtons: UIStackView = {
        let stack = UIStackView(frame: CGRect.zero)
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 20
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
    
    
    
    


    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        initComponents()
    }

    func initComponents() {
        addComponents()
        setAutolayout()
        activateCurrentLayout()
        setDelegates()
        addObservers()

    }

    func addComponents() {

       
        addSubview(topImage)
        addSubview(midImage)
        
        addSubview(stackViewForButtons)
        stackViewForButtons.addArrangedSubview(emailTextField)
        stackViewForButtons.addArrangedSubview(passwordTextField)
        stackViewForButtons.addArrangedSubview(iniciarSesionButton)
        emailTextField.addTarget(self, action :#selector(typingEmail), for:UIControl.Event.editingChanged)
        passwordTextField.addTarget(self, action :#selector(typingPassword), for:UIControl.Event.editingChanged)
        
    }
    
    

    func setAutolayout() {
        // Compact Constraints
        compactConstraints = [
            topImage.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            topImage.leftAnchor.constraint(equalTo: self.leftAnchor),
            topImage.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            topImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3),
            
            midImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -100),
            midImage.centerXAnchor.constraint(equalTo: centerXAnchor),
            midImage.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            midImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15),
            
            stackViewForButtons.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            stackViewForButtons.topAnchor.constraint(equalTo: midImage.bottomAnchor, constant: 21),
            stackViewForButtons.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.7),
            stackViewForButtons.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3),
        ]

        // Regular Constraints
        regularConstraints = [

            topImage.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            topImage.leftAnchor.constraint(equalTo: self.leftAnchor),
            topImage.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            topImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3),
            
            midImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -100),
            midImage.centerXAnchor.constraint(equalTo: centerXAnchor),
            midImage.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            midImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15),
            
            stackViewForButtons.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            stackViewForButtons.topAnchor.constraint(equalTo: midImage.bottomAnchor, constant: 21),
            stackViewForButtons.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.7),
            stackViewForButtons.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3),
        ]

        // Large Constraints
        largeConstraints = [
            
            topImage.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            topImage.leftAnchor.constraint(equalTo: self.leftAnchor),
            topImage.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            topImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3),
            
            midImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -100),
            midImage.centerXAnchor.constraint(equalTo: centerXAnchor),
            midImage.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            midImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15),
            
            stackViewForButtons.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            stackViewForButtons.topAnchor.constraint(equalTo: midImage.bottomAnchor, constant: 21),
            stackViewForButtons.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.7),
            stackViewForButtons.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3),
            
            

        ]
    }

    func activateCurrentLayout() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                // iPhone 5 or 5S or 5C
                NSLayoutConstraint.activate(compactConstraints)
            case 1334:
                // iPhone 6/6S/7/8
                NSLayoutConstraint.activate(regularConstraints)

            case 2208:
                // iPhone 6+/6s+/7+/8+
                NSLayoutConstraint.activate(regularConstraints)
            case 2436:
                // iPhone X/Xs/11 Pro
                NSLayoutConstraint.activate(largeConstraints)
            case 1792:
                // iPhone 11/XR
                NSLayoutConstraint.activate(largeConstraints)
            default:
                // 11 Pro Max/Xs Max
                NSLayoutConstraint.activate(largeConstraints)
            }
        } else if UIDevice().userInterfaceIdiom == .pad {
            // iPad cases
            NSLayoutConstraint.activate(regularConstraints)
        }
    }

    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(didKeyboardShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didKeyboardShow), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
        
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
        
    @objc
    func didKeyboardShow(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = self.convert(keyboardScreenEndFrame, from: self.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            isKeyboardOpen(true, keyboardFrame: keyboardViewEndFrame)
        } else {
            isKeyboardOpen(false, keyboardFrame: keyboardViewEndFrame)
        }
    }
        
    func isKeyboardOpen(_ isOpen: Bool, keyboardFrame: CGRect) {
        
        if isOpen {
            midImage.transform = CGAffineTransform.identity
            topImage.transform = CGAffineTransform.identity
            emailTextField.transform = CGAffineTransform.identity
            passwordTextField.transform = CGAffineTransform.identity
        } else {
            
            let modelName = UIDevice.current.modelName
            
            if modelName == "iPhone 7" || modelName == "iPhone 8" || modelName == "iPhone 6" || modelName == "iPhone 6s" || modelName == "iPhone 8 Plus" || modelName == "iPhone 7 Plus" || modelName == "iPhone 6s Plus" || modelName == "iPhone SE"{
               emailTextField.transform = CGAffineTransform(translationX: 0, y: -(keyboardFrame.height - 220))
               passwordTextField.transform = CGAffineTransform(translationX: 0, y: -(keyboardFrame.height - 220))
               midImage.transform = CGAffineTransform(translationX: 0, y: -(keyboardFrame.height - 220))
                topImage.transform = CGAffineTransform(translationX: 0, y: -(keyboardFrame.height - 220))
            }
            else{
               emailTextField.transform = CGAffineTransform(translationX: 0, y: -(keyboardFrame.height - 240))
                passwordTextField.transform = CGAffineTransform(translationX: 0, y: -(keyboardFrame.height - 240))
                midImage.transform = CGAffineTransform(translationX: 0, y: -(keyboardFrame.height - 240))
                topImage.transform = CGAffineTransform(translationX: 0, y: -(keyboardFrame.height - 240))
            }
        }
    }
        
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailTextField.endEditing(true)
        passwordTextField.endEditing(true)
        topImage.endEditing(true)
        midImage.endEditing(true)
    }

}

extension LoginView {
    @objc private func goHomeBtn() {
        delegate?.goHome(self)
    }
    
    @objc private func typingEmail() {
        delegate?.typeEmail(self)
    }
    
    @objc private func typingPassword() {
        delegate?.typePassword(self)
    }
    
    


}



//
//  LoginViewModel.swift
//  ArquitectureLogin
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

protocol LoginViewProtocol {
    func getDataFromToken(data: ScheduleResponse)
    func getDataFromLogin(data: LoginResponse)
}

protocol LoginViewModelProtocol {
    func callTokenService(request: requestForTokenGuessStruct)
    func callLoginService(request: LoginRequest)
}

class LoginViewModel: LoginViewModelProtocol {
    func callLoginService(request: LoginRequest) {
        repository.callServiceLogin(request) { (response, error) in
            if error?.localizedDescription == "The Internet connection appears to be offline."{
                print("no hay internet")
                
            }
            if error == NetworkError.timeoutError {
                print("esperó 30s")
               

            }
            
            if error == NetworkError.nointernet {
                print("no hay internet")
                
            }
            
            if error == NetworkError.sinSalidaNetwork {
                print("no hay internet")
                
            }

            
            if let response = response {
                print("response")
                self.view.getDataFromLogin(data: response)
            }
            
        }
    }
    
    func callTokenService(request: requestForTokenGuessStruct) {
        repository.callServiceToken(request) { (response, error) in
            if error?.localizedDescription == "The Internet connection appears to be offline."{
                print("no hay internet")
                
            }
            if error == NetworkError.timeoutError {
                print("esperó 30s")
               

            }
            
            if error == NetworkError.nointernet {
                print("no hay internet")
                
            }
            
            if error == NetworkError.sinSalidaNetwork {
                
                
            }

            
            if let response = response {
                
                self.view.getDataFromToken(data: response)
            }
            
        }
    }
    
    
    var view: LoginViewProtocol
    var repository: LoginRepositoryProtocol
    var defaults: DefaultsManager
    
    init(view: LoginViewProtocol, repository: LoginRepositoryProtocol, defaults: DefaultsManager) {
        self.view = view
        self.repository = repository
        self.defaults = defaults
    }

    
}


//
//  SelectedShowFactory.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/1/21.
//

import Foundation
import UIKit

protocol SelectedShowFactory {
    func makeSelectedShowViewModel(_ view: SelectedShowViewProtocol, repository: SelectedShowRepositoryProtocol) -> SelectedShowViewModelProtocol
    func makeSelectedShowRepository() -> SelectedShowRepository
    func makeSelectedShowView(_ bgColor: UIColor) -> SelectedShowView
    func makeSelectedShowVC(_ view: SelectedShowView, coordinator: MainCoordinator) -> SelectedShowVC
}

extension DependencyContainer: SelectedShowFactory {
    

    
    func makeSelectedShowViewModel(_ view: SelectedShowViewProtocol, repository: SelectedShowRepositoryProtocol) -> SelectedShowViewModelProtocol {
        let viewModel = SelectedShowViewModel(view: view, repository: repository, defaults: defaults)
        
        return viewModel
    }
    
    func makeSelectedShowRepository() -> SelectedShowRepository {
        return SelectedShowRepository(networkService: networkService, localService: localService)
    }
    
    func makeSelectedShowView(_ bgColor: UIColor) -> SelectedShowView {
        let view = SelectedShowView()
        view.backgroundColor = bgColor
        
        return view
    }
    
    func makeSelectedShowVC(_ view: SelectedShowView, coordinator: MainCoordinator) -> SelectedShowVC {
        let vc = SelectedShowVC(UI: view, factory: self, coordinator: coordinator, defaults: defaults)
        
        return vc
    }

}

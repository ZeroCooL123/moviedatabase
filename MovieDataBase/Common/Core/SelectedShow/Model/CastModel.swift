//
//  CastModel.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/1/21.
//

import Foundation
struct CastResponse: Initiable {

    let success: Bool?
    let status_code: String?
    let status_message: String?
    let cast: [CastStruct]?

    init() {
        self.success = false
        self.status_code = ""
        self.status_message = ""
        self.cast = [CastStruct]()
    }
}



struct CastStruct: Initiable {
    
    let original_name: String?
    let character: String?
    let profile_path: String?
    

    init() {
        
        self.original_name = ""
        self.character = ""
        self.profile_path = ""


        }
    
    init(dictionary: NSDictionary) {
        self.original_name = dictionary["original_name"] as? String ?? ""
        self.character = dictionary["character"] as? String ?? ""
        self.profile_path = dictionary["profile_path"] as? String ?? ""

    }
    
}



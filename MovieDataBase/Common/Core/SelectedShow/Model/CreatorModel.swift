//
//  CreatorModel.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/1/21.
//

import Foundation

struct CreatorModelStruct: Initiable {

    let success: Bool?
    let status_code: String?
    let status_message: String?
    let created_by: [CreatedStruct]?
    let last_episode_to_air: EpisodeStructForCreator

    init() {
        self.success = false
        self.status_code = ""
        self.status_message = ""
        self.created_by = [CreatedStruct]()
        self.last_episode_to_air = EpisodeStructForCreator()
    }
}

struct EpisodeStructForCreator: Initiable {
    let season_number: String?
    let still_path: String?
    let air_date: String?
    
    init() {
        
            self.season_number = ""
            self.still_path = ""
            self.air_date = ""
        
        }
    
    init(dictionary: NSDictionary) {
        self.season_number = dictionary["season_number"] as? String ?? ""
        self.still_path = dictionary["still_path"] as? String ?? ""
        self.air_date = dictionary["air_date"] as? String ?? ""
        
    }
    
}



struct CreatedStruct: Initiable {
    
    let name: String?
    
    

    init() {
        
        self.name = ""
        


        }
    
    init(dictionary: NSDictionary) {
        self.name = dictionary["name"] as? String ?? ""
        
    }
    
}



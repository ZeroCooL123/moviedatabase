//
//  SeasonModel.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/2/21.
//

import Foundation

struct SeasonModelStruct: Initiable {

    let success: Bool?
    let status_code: String?
    let status_message: String?
    let episodes: [EpisodesStruct]?
    

    init() {
        self.success = false
        self.status_code = ""
        self.status_message = ""
        self.episodes = [EpisodesStruct]()
        
    }
}

struct EpisodesStruct: Initiable {
    let episode_number: Int?
    let name: String?
    let still_path: String?
    let seasonNumerEpisode: Int?
    init() {
        
            self.episode_number = 0
            self.name = ""
            self.still_path = ""
            self.seasonNumerEpisode = 0
        }
    
    init(dictionary: NSDictionary, seasonInt: Int) {
        self.episode_number = dictionary["episode_number"] as? Int ?? 0
        self.name = dictionary["name"] as? String ?? ""
        self.still_path = dictionary["still_path"] as? String ?? ""
        self.seasonNumerEpisode = seasonInt
    }
    
}



struct ArregloBidimencionalStruct: Initiable {
    let arregloDeEpisodios: [EpisodesStruct]?
    let seasonNumber: Int?
    
    init() {
        
        self.arregloDeEpisodios = [EpisodesStruct]()
        self.seasonNumber = 0
        
        }
    
    init(arreglo: [EpisodesStruct], tag: Int) {
        self.arregloDeEpisodios = arreglo
        self.seasonNumber = tag
    }

}

struct ArregloTridimencionalStruct: Initiable {
    let arregloDeArreglos: [ArregloBidimencionalStruct]?
    let seasonNumberArreglo: Int?
    
    init() {
        
        self.arregloDeArreglos = [ArregloBidimencionalStruct]()
        self.seasonNumberArreglo = 0
        
        }
    
    init(arregloTri: [ArregloBidimencionalStruct], tag: Int) {
        self.arregloDeArreglos = arregloTri
        self.seasonNumberArreglo = tag
    }

}


extension SelectedShowVC{
    func theFunction(){
        if arraySeasonsEpisodes.isEmpty == false {
            if !called {
                called = true
                let arreglo = arraySeasonsEpisodes
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 0)
                arregloDeArreglos.insert([arregloDeArregloss], at: 0)
            }
        }
   
    }
    
    func theFunction2(){
        
        if arraySeasonsEpisodes2.isEmpty == false {
            if !called2 {
                called2 = true
                let arreglo = arraySeasonsEpisodes2
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 1)
                arregloDeArreglos.insert([arregloDeArregloss], at: 1)
            }
        }
    
    }
    func theFunction3(){
        
        if arraySeasonsEpisodes3.isEmpty == false {
            if !called3 {
                called3 = true
                let arreglo = arraySeasonsEpisodes3
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 2)
                arregloDeArreglos.insert([arregloDeArregloss], at: 2)
            }
        }
    
    }
    func theFunction4(){
        
        if arraySeasonsEpisodes4.isEmpty == false {
            if !called4 {
                called4 = true
                let arreglo = arraySeasonsEpisodes4
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 3)
                arregloDeArreglos.insert([arregloDeArregloss], at: 3)
            }
        }
    
    }
    func theFunction5(){
        
        if arraySeasonsEpisodes5.isEmpty == false {
            if !called5 {
                called5 = true
                let arreglo = arraySeasonsEpisodes5
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 4)
                arregloDeArreglos.insert([arregloDeArregloss], at: 4)
            }
        }
    
    }
    func theFunction6(){
        if arraySeasonsEpisodes6.isEmpty == false {
            if !called6 {
                called6 = true
                let arreglo = arraySeasonsEpisodes6
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 5)
                arregloDeArreglos.insert([arregloDeArregloss], at: 5)
            }
        }
   
    }
    
    func theFunction7(){
        
        if arraySeasonsEpisodes7.isEmpty == false {
            if !called7 {
                called7 = true
                let arreglo = arraySeasonsEpisodes7
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 6)
                arregloDeArreglos.insert([arregloDeArregloss], at: 6)
            }
        }
    
    }
    func theFunction8(){
        
        if arraySeasonsEpisodes8.isEmpty == false {
            if !called8 {
                called8 = true
                let arreglo = arraySeasonsEpisodes8
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 7)
                arregloDeArreglos.insert([arregloDeArregloss], at: 7)
            }
        }
    
    }
    func theFunction9(){
        
        if arraySeasonsEpisodes9.isEmpty == false {
            if !called9 {
                called9 = true
                let arreglo = arraySeasonsEpisodes9
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 8)
                arregloDeArreglos.insert([arregloDeArregloss], at: 8)
            }
        }
    
    }
    func theFunction10(){
        
        if arraySeasonsEpisodes10.isEmpty == false {
            if !called10 {
                called10 = true
                let arreglo = arraySeasonsEpisodes10
                let arregloDeArregloss = ArregloBidimencionalStruct(arreglo: arreglo, tag: 9)
                arregloDeArreglos.insert([arregloDeArregloss], at: 9)
            }
        }
    
    }
   
    func removeAllArrayInfo(){
        arrayCast.removeAll()
        arrayCreator.removeAll()
        arrayOfSeasonsInt.removeAll()
        arrayCreatorName.removeAll()
        arraySeasonsEpisodes.removeAll()
        arregloDeArreglos.removeAll()
        arraySeasonsEpisodes.removeAll()
        arraySeasonsEpisodes2.removeAll()
        arraySeasonsEpisodes3.removeAll()
        arraySeasonsEpisodes4.removeAll()
        arraySeasonsEpisodes5.removeAll()
        arraySeasonsEpisodes6.removeAll()
        arraySeasonsEpisodes7.removeAll()
        arraySeasonsEpisodes8.removeAll()
        arraySeasonsEpisodes9.removeAll()
        arraySeasonsEpisodes10.removeAll()
        called = false
        called2 = false
        called3 = false
        called4 = false
        called5 = false
        called6 = false
        called7 = false
        called8 = false
        called9 = false
        called10 = false
    }
}
extension SelectedShowVC{
    func getCastInfo(url: String){
        var semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            self.coordinator.hideLoader(nil)
            semaphore.signal()
            return
          }
          semaphore.signal()
            do {
                 let json = try JSONSerialization.jsonObject(with: data)
                 if let jsonArray = json as? [[String:Any]] {
                   // print("json is array", jsonArray)
                 } else if let jsonDictionary = json as? NSDictionary {
                    let toneCategories = jsonDictionary["cast"] as? [NSDictionary] ?? []

                       for category in toneCategories {
                        let show = CastStruct(dictionary: category)
                        arrayCast.append(show)
                       }
                    DispatchQueue.main.async {
                        self.UI.castCollectionView.reloadData()
                    }
                   
                    
                 } else {
                    print("This should never be displayed")
                 }
            }
            catch let error as NSError {
                self.coordinator.hideLoader(nil)
                print(error.localizedDescription)
            }
            
        }

        task.resume()
        semaphore.wait()
    }
    
    
    
    func getCreators(url: String){
        var semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            self.coordinator.hideLoader(nil)
            semaphore.signal()
            return
          }
          semaphore.signal()
            do {
                 let json = try JSONSerialization.jsonObject(with: data)
                 if let jsonArray = json as? [[String:Any]] {
                    //print("json is array", jsonArray)
                 } else if let jsonDictionary = json as? NSDictionary {
                    //print("json is dictionary", jsonDictionary)
                    let toneCategories = jsonDictionary["created_by"] as? [NSDictionary] ?? []
                    let toneCategories2 = jsonDictionary["last_episode_to_air"] as? NSDictionary ?? nil
                    let season_numberr = toneCategories2?["season_number"] as? Int
                    let still_pathh = toneCategories2?["still_path"] as? String
                    let air_datee = toneCategories2?["air_date"] as? String
                    
                    season_number = String(season_numberr ?? 0)
                    still_path = still_pathh ?? ""
                    air_date = air_datee ?? ""
                    
                    
                       for category in toneCategories {
                        let show = CreatedStruct(dictionary: category)
                        arrayCreator.append(show)
                        
                        let descrp = category["name"]
                        arrayCreatorName.append(descrp as? String ?? "")
                       }
                    
    
                    DispatchQueue.main.async {
                        
                        let number = Int(season_number) ?? 0
                        let reversedCollection = (1 ... number).reversed()

                        for index in reversedCollection {
                            //arreglo para las temporadas
                            arrayOfSeasonsInt.append(index)
                            
                        }
                        let urlImage = still_path
                        let imagenn = "https://image.tmdb.org/t/p/w500\(urlImage)"
                        self.UI.posterLastSeason.downloaded(from: imagenn, contentMode: .scaleToFill)

                        self.UI.seasonNumber.text = "Season \(season_number)"
                        self.UI.lastDateSeason.text = air_date
                        
                        let formatter = ListFormatter()
                        if let string = formatter.string(from: arrayCreatorName) {
                            self.UI.creatorLabel.text = "Created by: \(string)"
                            
                        }
                        //self.coordinator.hideLoader(nil)
                    }
                    DispatchQueue.main.async { [self] in
                        let arregloparaSeasons = arrayOfSeasonsInt.count
                       
                        for index in 1...arregloparaSeasons {
                            
                            getSeasons(url: "https://api.themoviedb.org/3/tv/\(UI.SelectedIDD)/season/\(index)?api_key=8d9ced07152c868de3f180fef43510a7&language=en-US")
                        }
                        
                        self.coordinator.hideLoader(nil)
                    }

   
                 } else {
                    print("This should never be displayed")
                 }
            }
            catch let error as NSError {
                self.coordinator.hideLoader(nil)
                print(error.localizedDescription)
            }
            
        }

        task.resume()
        semaphore.wait()
        
    }
    
    
    func getSeasons(url: String){
        var semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            self.coordinator.hideLoader(nil)
            semaphore.signal()
            return
          }
          semaphore.signal()
            do {
                 let json = try JSONSerialization.jsonObject(with: data)
                if json is [[String:Any]] {
                   // print("json is array", jsonArray)
                 } else if let jsonDictionary = json as? NSDictionary {
                    let toneCategories = jsonDictionary["episodes"] as? [NSDictionary] ?? []
                    let season = jsonDictionary["season_number"] as? Int ?? 0

                    for category in toneCategories {
                        if season == 1{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes.append(show)
                            
                            
                        }else if season == 2{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes2.append(show)
                        }
                        if season == 3{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes3.append(show)
                        }else if season == 4{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes4.append(show)
                        }
                        if season == 5{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes5.append(show)
                        }else if season == 6{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes6.append(show)
                        }
                        if season == 7{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes7.append(show)
                        }else if season == 8{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes8.append(show)
                        }
                        if season == 9{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes9.append(show)
                        }else if season == 10{
                            let show = EpisodesStruct(dictionary: category, seasonInt: season)
                            arraySeasonsEpisodes10.append(show)
                        }

                    }
                    

                    DispatchQueue.main.async { [self] in
                        self.UI.tableSeasons.reloadData()
                        self.coordinator.hideLoader(nil)
                        if arraySeasonsEpisodes.isEmpty == false {
                            
                            self.theFunction()
                        }
                        if arraySeasonsEpisodes2.isEmpty == false{
                            self.theFunction2()
                        }
                        if arraySeasonsEpisodes3.isEmpty == false{
                            self.theFunction3()
                        }
                        if arraySeasonsEpisodes4.isEmpty == false{
                            self.theFunction4()
                        }
                        if arraySeasonsEpisodes5.isEmpty == false{
                            self.theFunction5()
                        }
                        if arraySeasonsEpisodes6.isEmpty == false {
                            
                            self.theFunction6()
                        }
                        if arraySeasonsEpisodes7.isEmpty == false{
                            self.theFunction7()
                        }
                        if arraySeasonsEpisodes8.isEmpty == false{
                            self.theFunction8()
                        }
                        if arraySeasonsEpisodes9.isEmpty == false{
                            self.theFunction9()
                        }
                        if arraySeasonsEpisodes10.isEmpty == false{
                            self.theFunction10()
                        }
                        
                        
                    }
                 } else {
                    print("This should never be displayed")
                 }
            }
            catch let error as NSError {
                self.coordinator.hideLoader(nil)
                print(error.localizedDescription)
                
                
            }
            
        }

        task.resume()
        semaphore.wait()
    }
      
}

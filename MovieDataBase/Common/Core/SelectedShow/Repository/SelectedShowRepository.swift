//
//  SelectedShowRepository.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/1/21.
//

import Foundation

import Foundation

protocol SelectedShowRepositoryProtocol: WebRepository {
    
}

class SelectedShowRepository: SelectedShowRepositoryProtocol {
    
    
    

    var localService: LocalManager
    var networkService: NetworkManager
    
    
    
    required init(networkService: NetworkManager, localService: LocalManager) {
        self.networkService = networkService
        self.localService = localService
    }

}


//
//  CastCell.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/1/21.
//

import UIKit

class CastCell: UICollectionViewCell {
    var model: CastStruct? {
        didSet {
            guard let viewModel = model else { return }
            let guardado = viewModel.profile_path
            
            originalName.text = viewModel.original_name
            character.text = viewModel.character

            let image = "https://image.tmdb.org/t/p/w500\(guardado ?? "")"
            if image == "" || image == "undefined" {
                imagenCast.image = UIImage(named: "")
                imagenCast.contentMode = .scaleAspectFill
            }else{
                imagenCast.downloaded(from: image)
                imagenCast.contentMode = .scaleAspectFill
            }
        }
    }
    
    
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    let imagenCast: UIImageView = {
        
        let catImage = UIImage()
        let myImageView:UIImageView = UIImageView()
        myImageView.clipsToBounds = true
        myImageView.frame.size.width = 100
        myImageView.frame.size.height = 100
        myImageView.image = catImage
        myImageView.layer.cornerRadius = 40
        myImageView.layer.borderWidth = 1
        myImageView.layer.borderColor = UIColor.clear.cgColor
        myImageView.contentMode = .scaleAspectFill
        myImageView.translatesAutoresizingMaskIntoConstraints = false
        return myImageView
        
    }()
    
    var originalName: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = Fonts.AVENIR.of(size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        return label
    
        }()
    
    var character: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = Fonts.AVENIR.of(size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        return label
    
        }()
    
    
   
    

    
    func setupViews() {
        addSubview(imagenCast)
        addSubview(originalName)
        addSubview(character)

        
        
        NSLayoutConstraint.activate([
            imagenCast.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            imagenCast.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            imagenCast.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5),
            imagenCast.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5),
            
            
            originalName.leftAnchor.constraint(equalTo: imagenCast.leftAnchor),
            originalName.topAnchor.constraint(equalTo: imagenCast.bottomAnchor, constant: 8),
            character.leftAnchor.constraint(equalTo: originalName.leftAnchor),
            character.topAnchor.constraint(equalTo: originalName.bottomAnchor, constant: 8),
      
        ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

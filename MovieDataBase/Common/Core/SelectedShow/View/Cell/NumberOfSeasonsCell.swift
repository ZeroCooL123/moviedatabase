//
//  NumberOfSeasonsCell.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/2/21.
//

import UIKit


class NumberOfSeasonsCell: UITableViewCell, ViewDelegate {

    
    var compactConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var regularConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var largeConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    
    
    var seasonNumber: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = Fonts.AVENIR.of(size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        return label
    
        }()
    
    lazy var seasonsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 30
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.clear
        cv.register(SeasonCell.self, forCellWithReuseIdentifier: "identifierSeasons")
        cv.showsHorizontalScrollIndicator = false
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func contenido(array: ArregloBidimencionalStruct){
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initComponents()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {


        seasonsCollectionView.delegate = dataSourceDelegate
        seasonsCollectionView.dataSource = dataSourceDelegate
        seasonsCollectionView.tag = row
        seasonsCollectionView.setContentOffset(seasonsCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        seasonsCollectionView.reloadData()
    }
    

    

    var collectionViewOffset: CGFloat {
        set { seasonsCollectionView.contentOffset.x = newValue }
        get { return seasonsCollectionView.contentOffset.x }
    }
    

    
    func initComponents() {
        addComponents()
        setAutolayout()
        activateCurrentLayout()
        
    }
    
    func addComponents() {
        self.addSubview(seasonsCollectionView)
        self.addSubview(seasonNumber)
    }
    
    func setAutolayout() {
        largeConstraints = [
            
            
            seasonsCollectionView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            seasonsCollectionView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            seasonsCollectionView.widthAnchor.constraint(equalTo: self.widthAnchor),
            seasonsCollectionView.heightAnchor.constraint(equalTo: self.heightAnchor),
            seasonNumber.bottomAnchor.constraint(equalTo: seasonsCollectionView.topAnchor, constant: 28),
            seasonNumber.leftAnchor.constraint(equalTo: seasonsCollectionView.leftAnchor),
        ]
    }
    
    func activateCurrentLayout() {
        NSLayoutConstraint.activate(largeConstraints)
    }

}

extension UITableViewCell {
    open override func addSubview(_ view: UIView) {
        super.addSubview(view)
        sendSubviewToBack(contentView)
    }
}

//
//  SeasonCell.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/2/21.
//

import UIKit

class SeasonCell: UICollectionViewCell {
    
    var model: EpisodesStruct? {
        didSet {
            guard let viewModel = model else { return }

            let episodio = viewModel.episode_number
            let nombre = viewModel.name
            let guardado = viewModel.still_path
            episodeNumber.text = "Episode Number: \(episodio ?? 0)"
            episodeName.text = "Episode Name: \(nombre ?? "")"

            let image = "https://image.tmdb.org/t/p/w500\(guardado ?? "")"
            if image == "" || image == "undefined" {
                seasonImage.image = UIImage()
                seasonImage.contentMode = .scaleAspectFill
            }else{
                seasonImage.downloaded(from: image)
                seasonImage.contentMode = .scaleAspectFill
            }
            
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }

    
    let seasonImage: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .clear
        iv.image = UIImage(named: "")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    var episodeNumber: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = Fonts.AVENIR.of(size: 10)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        return label
    
        }()
    
    var episodeName: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = Fonts.AVENIR.of(size: 10)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        return label
    
        }()
    
    func setupViews() {
        addSubview(seasonImage)
        addSubview(episodeNumber)
        addSubview(episodeName)

        
        
        NSLayoutConstraint.activate([
            seasonImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            seasonImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            seasonImage.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1),
            seasonImage.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            
            
            episodeNumber.leftAnchor.constraint(equalTo: seasonImage.leftAnchor),
            episodeNumber.topAnchor.constraint(equalTo: seasonImage.bottomAnchor, constant: 8),
            episodeNumber.rightAnchor.constraint(equalTo: seasonImage.rightAnchor),
            episodeName.leftAnchor.constraint(equalTo: episodeNumber.leftAnchor),
            episodeName.rightAnchor.constraint(equalTo: seasonImage.rightAnchor),
            episodeName.topAnchor.constraint(equalTo: episodeNumber.bottomAnchor, constant: 8),
      
        ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//
//  SelectedShowVC.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/1/21.
//

import UIKit
import Darwin

var storedOffsets = [Int: CGFloat]()
let model = arregloDeArreglos
class SelectedShowVC: UIViewController {

    // MARK: - Controller's properties

    let UI: SelectedShowView
    let coordinator: MainCoordinator
    let defaults: DefaultsManager
    private var factory: SelectedShowFactory
    lazy var viewModel: SelectedShowViewModelProtocol = {
        factory.makeSelectedShowViewModel(self, repository: factory.makeSelectedShowRepository())
    }()
    var called: Bool = false
    var called2: Bool = false
    var called3: Bool = false
    var called4: Bool = false
    var called5: Bool = false
    var called6: Bool = false
    var called7: Bool = false
    var called8: Bool = false
    var called9: Bool = false
    var called10: Bool = false

   
    
    // MARK: - Controller's initializers

    required init(UI: SelectedShowView, factory: SelectedShowFactory, coordinator: MainCoordinator, defaults: DefaultsManager) {
        self.UI = UI
        self.factory = factory
        self.coordinator = coordinator
        self.defaults = defaults

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK: - Controller's life cycle

    override func loadView() {
        self.view = self.UI
        UI.delegate = self
        
        
    }

    override func viewDidLayoutSubviews() {
        setDelegates()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
       
       
    }

    override func viewWillAppear(_ animated: Bool) {
        removeAllArrayInfo()
        
        DispatchQueue.main.async { [self] in
            if InternetConnectionManager.isConnectedToNetwork() == true{
                self.UI.posterHeader.image = UIImage()
                let urlImage = self.UI.SelectedBackdropp
                let imagenn = "https://image.tmdb.org/t/p/w500\(urlImage)"
                self.UI.posterHeader.downloaded(from: imagenn, contentMode: .scaleToFill)
                coordinator.showLoader()
            }
            else{
                self.view.makeToast(message: "No internet connection")
            }
            
        }
        DispatchQueue.main.async { [self] in
            if InternetConnectionManager.isConnectedToNetwork() == true{
                getCastInfo(url: "https://api.themoviedb.org/3/tv/\(UI.SelectedIDD)/credits?api_key=8d9ced07152c868de3f180fef43510a7&language=en-US")
            }
            else{
                self.view.makeToast(message: "No internet connection")
            }
            
            
        }
        DispatchQueue.main.async { [self] in
            
            if InternetConnectionManager.isConnectedToNetwork() == true{
                getCreators(url: "https://api.themoviedb.org/3/tv/\(UI.SelectedIDD)?api_key=8d9ced07152c868de3f180fef43510a7&language=en-US")
            }
            else{
                self.view.makeToast(message: "No internet connection")
            }
            
        }
        
        

        
        UI.tvTitleLabel.text = UI.SelectedNamee
        UI.descriptionTV.text = UI.SelectedOverVieww
        UI.scoreLabel.text = UI.SelectedVotee
       
    }

    override func viewDidAppear(_ animated: Bool) {}

    override func viewWillDisappear(_ animated: Bool) {
    }

    override func viewDidDisappear(_ animated: Bool) {}

    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }
    
    private func setDelegates() {
        UI.castCollectionView.delegate = self
        UI.castCollectionView.dataSource = self
        UI.tableSeasons.delegate = self
        UI.tableSeasons.dataSource = self
        
    }

   
}

extension SelectedShowVC: SelectedShowViewProtocol {
    
    

    
    
}

extension SelectedShowVC: SelectedShowViewDelegate {
    func dimiz(_ view: SelectedShowView) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    
    func popularBTN(_ view: SelectedShowView) {
        
    }
    
    
    

}

extension SelectedShowVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == UI.castCollectionView{
            return arrayCast.count
        }else{
            return arraySeasonsEpisodes.count
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == UI.castCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "identifierCast", for: indexPath) as! CastCell
        
            
            cell.backgroundColor = .clear
            cell.model = arrayCast[indexPath.item]
            
            return cell
        }else{

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "identifierSeasons", for: indexPath) as! SeasonCell
            cell.backgroundColor = .clear
            
            let episodeArray = arregloDeArreglos[collectionView.tag][indexPath.section].arregloDeEpisodios ?? []
            let quesera = episodeArray[indexPath.item]
            cell.model = quesera
            
            return cell
        }
        
    
}
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView == UI.castCollectionView{
            return CGSize(width: 180, height: 200)
        }else{
            return CGSize(width: 180, height: 200)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
       
          
        if collectionView == UI.castCollectionView{
            return 30
        }else{
            return 30
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == UI.castCollectionView{
            print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        }else{
            print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        }
  
    }
    
    
    
}



extension SelectedShowVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return arrayOfSeasonsInt.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "seasonsRegister", for: indexPath) as? NumberOfSeasonsCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.seasonNumber.text = "Season \(indexPath.row + 1)"
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }

     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        guard let tableViewCell = cell as? NumberOfSeasonsCell else { return }
        //this method is for saving the offset and delegate for each cell
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }

     func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //this method is for saving the offset and delegate for each cell
        guard let tableViewCell = cell as? NumberOfSeasonsCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    
    
        
  
    
}



//
//  SelectedShowView.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/1/21.
//

import Foundation
import UIKit


protocol SelectedShowViewDelegate: NSObjectProtocol {
    func popularBTN(_ view: SelectedShowView)
    func dimiz(_ view: SelectedShowView)

}



class SelectedShowView: UIView, ViewDelegate {
 


    var SelectedNamee: String = ""
    var SelectedPosterr: String = ""
    var SelectedDateAirr: String = ""
    var SelectedOverVieww: String = ""
    var SelectedBackdropp: String = ""
    var SelectedVotee: String = ""
    var SelectedIDD: String = ""

    weak var delegate: SelectedShowViewDelegate?

    
    
    var compactConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var regularConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var largeConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()

    
    var arrowLeft: UIButton = {
          
         let textTitle = UIButton()
           textTitle.setTitle("", for: .normal)
           textTitle.titleLabel?.font = UIFont(name: "Helvetica", size: 9)
           textTitle.translatesAutoresizingMaskIntoConstraints = false
           textTitle.setTitleColor(UIColor.white, for: .normal)
           textTitle.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
           textTitle.titleLabel?.adjustsFontSizeToFitWidth = true
           textTitle.titleLabel?.adjustsFontForContentSizeCategory = true
           
        let image = UIImage(named: "leftArrow")?.withTintColor(.white)
           textTitle.setImage(image, for: .normal)
           textTitle.addTarget(self, action: #selector(goBack), for: .touchUpInside)
           return textTitle
           
       }()
    
    let posterHeader: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = .clear
        iv.image = UIImage(named: "")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    
    
    lazy var container: UIView = {
        let container = UIView(frame: CGRect.zero)
        container.backgroundColor = UIColor.mainDark
        container.layer.cornerRadius = 10
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    lazy var container2: UIView = {
        let container = UIView(frame: CGRect.zero)
        container.backgroundColor = UIColor.mainDark
        container.layer.cornerRadius = 10
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    
    let scrollContainer: UIScrollView = {
        let scrollView = UIScrollView(frame: CGRect.zero)
        scrollView.backgroundColor = UIColor.clear
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isScrollEnabled = true
        scrollView.isUserInteractionEnabled = true
        scrollView.layer.zPosition = 2
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 0, height: 1200)
        return scrollView
    }()
    
    
    var labelSummary: UILabel = {
        let label = UILabel()
        label.text = "Summary"
        label.numberOfLines = 1
        label.font = Fonts.AVENIRBold.of(size: 14)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = UIColor.algaeGreen
        return label
    
        }()
    
    var tvTitleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.font = Fonts.AVENIRBold.of(size: 14)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = UIColor.white
        return label
    
        }()
    
    var descriptionTV: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 0
        label.font = Fonts.AVENIRBold.of(size: 12)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .justified
        label.textColor = UIColor.white
        return label
    
        }()
    
    var creatorLabel: UILabel = {
        let label = UILabel()
        label.text = "Created by"
        label.numberOfLines = 0
        label.font = Fonts.AVENIRBold.of(size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = UIColor.white
        return label
    
        }()
    
    let greenCircle: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        iv.backgroundColor = .clear
        iv.image = UIImage(named: "greenCircle")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    var scoreLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.font = Fonts.AVENIRBold.of(size: 14)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = UIColor.white
        return label
    
        }()
    
    lazy var imageContainer: UIView = {
        let container = UIView(frame: CGRect.zero)
        container.backgroundColor = UIColor.mainDark
        container.layer.cornerRadius = 0
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    let posterLastSeason: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = .clear
        iv.isUserInteractionEnabled = true
        iv.image = UIImage(named: "")
        iv.translatesAutoresizingMaskIntoConstraints = false
        
        return iv
    }()
    
    var seasonNumber: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.font = Fonts.AVENIRBold.of(size: 14)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.textColor = UIColor.white
        return label
    
        }()
    var lastDateSeason: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 1
        label.font = Fonts.AVENIRBold.of(size: 14)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.textColor = UIColor.algaeGreen
        return label
    
        }()
    
    
    
    lazy var castCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 20
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.clear
        cv.register(CastCell.self, forCellWithReuseIdentifier: "identifierCast")
        cv.isPagingEnabled = true
     cv.showsVerticalScrollIndicator = false
     cv.showsHorizontalScrollIndicator = false
     cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    
    
    let tableSeasons: UITableView = {
        let table = UITableView(frame: CGRect.zero)
        table.register(NumberOfSeasonsCell.self, forCellReuseIdentifier: "seasonsRegister")
        table.backgroundColor = .clear

        table.layer.cornerRadius = 0
        table.separatorColor = .clear
        table.translatesAutoresizingMaskIntoConstraints = false
        table.showsHorizontalScrollIndicator = false
        table.showsVerticalScrollIndicator = false
        return table
    }()
    


    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        initComponents()
    }

    func initComponents() {
        addComponents()
        setAutolayout()
        activateCurrentLayout()
        setDelegates()
       
        

    }
    

    func addComponents() {
        
        addSubview(posterHeader)
        addSubview(arrowLeft)
    
        addSubview(container)
        container.addSubview(greenCircle)
        greenCircle.addSubview(scoreLabel)
        container.addSubview(scrollContainer)
        scrollContainer.addSubview(labelSummary)
        scrollContainer.addSubview(tvTitleLabel)
        scrollContainer.addSubview(descriptionTV)
        scrollContainer.addSubview(creatorLabel)
        scrollContainer.addSubview(posterLastSeason)
        scrollContainer.addSubview(seasonNumber)
        scrollContainer.addSubview(lastDateSeason)
        scrollContainer.addSubview(castCollectionView)
        
        scrollContainer.addSubview(tableSeasons)
        
        
    }
    
    func setDelegates() {
        
    }


    func setAutolayout() {
        // Compact Constraints
        compactConstraints = [
            arrowLeft.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 32),
            arrowLeft.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 10),
            arrowLeft.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.025),
            arrowLeft.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.025),
            
            posterHeader.topAnchor.constraint(equalTo: self.topAnchor),
            posterHeader.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1),
            posterHeader.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.23),
            posterHeader.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            
            container.topAnchor.constraint(equalTo: posterHeader.bottomAnchor, constant: -32),
            container.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            container.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            container.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.85),
            
            greenCircle.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -32),
            greenCircle.topAnchor.constraint(equalTo: container.topAnchor, constant: -12),
            greenCircle.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.1),
            greenCircle.heightAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.1),
            
            scoreLabel.centerYAnchor.constraint(equalTo: greenCircle.centerYAnchor),
            scoreLabel.centerXAnchor.constraint(equalTo: greenCircle.centerXAnchor),
            
            scrollContainer.topAnchor.constraint(equalTo: container.topAnchor),
            scrollContainer.leftAnchor.constraint(equalTo: container.leftAnchor),
            scrollContainer.rightAnchor.constraint(equalTo: container.rightAnchor),
            scrollContainer.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            
            labelSummary.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            labelSummary.topAnchor.constraint(equalTo: scrollContainer.topAnchor, constant: 21),
            labelSummary.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),
            
            tvTitleLabel.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            tvTitleLabel.topAnchor.constraint(equalTo: labelSummary.bottomAnchor, constant: 12),
            tvTitleLabel.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),

            descriptionTV.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            descriptionTV.topAnchor.constraint(equalTo: tvTitleLabel.bottomAnchor, constant: 12),
            descriptionTV.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),

            creatorLabel.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            creatorLabel.topAnchor.constraint(equalTo: descriptionTV.bottomAnchor, constant: 12),
            creatorLabel.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),

            
            
            posterLastSeason.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            posterLastSeason.topAnchor.constraint(equalTo: creatorLabel.bottomAnchor, constant: 12),
            posterLastSeason.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2),
            posterLastSeason.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.45),
            


            seasonNumber.topAnchor.constraint(equalTo: posterLastSeason.topAnchor),
            seasonNumber.leftAnchor.constraint(equalTo: posterLastSeason.rightAnchor, constant: 12),
            

            lastDateSeason.topAnchor.constraint(equalTo: seasonNumber.bottomAnchor),
            lastDateSeason.leftAnchor.constraint(equalTo: posterLastSeason.rightAnchor, constant: 12),

            castCollectionView.topAnchor.constraint(equalTo: posterLastSeason.bottomAnchor, constant: 12),
            castCollectionView.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            castCollectionView.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),
            castCollectionView.heightAnchor.constraint(equalTo: scrollContainer.heightAnchor, multiplier: 0.35),


            
            tableSeasons.topAnchor.constraint(equalTo: castCollectionView.bottomAnchor, constant: 12),
            tableSeasons.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            tableSeasons.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),
            tableSeasons.heightAnchor.constraint(equalTo: scrollContainer.heightAnchor, multiplier: 0.45),
            
        ]

        // Regular Constraints
        regularConstraints = [
            arrowLeft.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 32),
            arrowLeft.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 10),
            arrowLeft.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.025),
            arrowLeft.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.025),
            
            posterHeader.topAnchor.constraint(equalTo: self.topAnchor),
            posterHeader.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1),
            posterHeader.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.23),
            posterHeader.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            
            container.topAnchor.constraint(equalTo: posterHeader.bottomAnchor, constant: -32),
            container.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            container.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            container.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.85),
            
            greenCircle.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -32),
            greenCircle.topAnchor.constraint(equalTo: container.topAnchor, constant: -12),
            greenCircle.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.1),
            greenCircle.heightAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.1),
            
            scoreLabel.centerYAnchor.constraint(equalTo: greenCircle.centerYAnchor),
            scoreLabel.centerXAnchor.constraint(equalTo: greenCircle.centerXAnchor),
            
            scrollContainer.topAnchor.constraint(equalTo: container.topAnchor),
            scrollContainer.leftAnchor.constraint(equalTo: container.leftAnchor),
            scrollContainer.rightAnchor.constraint(equalTo: container.rightAnchor),
            scrollContainer.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            
            labelSummary.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            labelSummary.topAnchor.constraint(equalTo: scrollContainer.topAnchor, constant: 21),
            labelSummary.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),
            
            tvTitleLabel.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            tvTitleLabel.topAnchor.constraint(equalTo: labelSummary.bottomAnchor, constant: 12),
            tvTitleLabel.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),

            descriptionTV.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            descriptionTV.topAnchor.constraint(equalTo: tvTitleLabel.bottomAnchor, constant: 12),
            descriptionTV.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),

            creatorLabel.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            creatorLabel.topAnchor.constraint(equalTo: descriptionTV.bottomAnchor, constant: 12),
            creatorLabel.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),

            
            
            posterLastSeason.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            posterLastSeason.topAnchor.constraint(equalTo: creatorLabel.bottomAnchor, constant: 12),
            posterLastSeason.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2),
            posterLastSeason.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.45),
            


            seasonNumber.topAnchor.constraint(equalTo: posterLastSeason.topAnchor),
            seasonNumber.leftAnchor.constraint(equalTo: posterLastSeason.rightAnchor, constant: 12),
            

            lastDateSeason.topAnchor.constraint(equalTo: seasonNumber.bottomAnchor),
            lastDateSeason.leftAnchor.constraint(equalTo: posterLastSeason.rightAnchor, constant: 12),

            castCollectionView.topAnchor.constraint(equalTo: posterLastSeason.bottomAnchor, constant: 12),
            castCollectionView.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            castCollectionView.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),
            castCollectionView.heightAnchor.constraint(equalTo: scrollContainer.heightAnchor, multiplier: 0.35),


            
            tableSeasons.topAnchor.constraint(equalTo: castCollectionView.bottomAnchor, constant: 12),
            tableSeasons.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            tableSeasons.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),
            tableSeasons.heightAnchor.constraint(equalTo: scrollContainer.heightAnchor, multiplier: 0.45),
            

        ]

        // Large Constraints
        largeConstraints = [
            
            arrowLeft.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 32),
            arrowLeft.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 10),
            arrowLeft.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.025),
            arrowLeft.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.025),
            
            posterHeader.topAnchor.constraint(equalTo: self.topAnchor),
            posterHeader.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1),
            posterHeader.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.23),
            posterHeader.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            
            container.topAnchor.constraint(equalTo: posterHeader.bottomAnchor, constant: -32),
            container.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            container.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            container.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.85),
            
            greenCircle.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -32),
            greenCircle.topAnchor.constraint(equalTo: container.topAnchor, constant: -12),
            greenCircle.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.1),
            greenCircle.heightAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.1),
            
            scoreLabel.centerYAnchor.constraint(equalTo: greenCircle.centerYAnchor),
            scoreLabel.centerXAnchor.constraint(equalTo: greenCircle.centerXAnchor),
            
            scrollContainer.topAnchor.constraint(equalTo: container.topAnchor),
            scrollContainer.leftAnchor.constraint(equalTo: container.leftAnchor),
            scrollContainer.rightAnchor.constraint(equalTo: container.rightAnchor),
            scrollContainer.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            
            labelSummary.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            labelSummary.topAnchor.constraint(equalTo: scrollContainer.topAnchor, constant: 21),
            labelSummary.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),
            
            tvTitleLabel.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            tvTitleLabel.topAnchor.constraint(equalTo: labelSummary.bottomAnchor, constant: 12),
            tvTitleLabel.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),

            descriptionTV.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            descriptionTV.topAnchor.constraint(equalTo: tvTitleLabel.bottomAnchor, constant: 12),
            descriptionTV.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),

            creatorLabel.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            creatorLabel.topAnchor.constraint(equalTo: descriptionTV.bottomAnchor, constant: 12),
            creatorLabel.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),

            
            
            posterLastSeason.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            posterLastSeason.topAnchor.constraint(equalTo: creatorLabel.bottomAnchor, constant: 12),
            posterLastSeason.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2),
            posterLastSeason.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.45),
            


            seasonNumber.topAnchor.constraint(equalTo: posterLastSeason.topAnchor),
            seasonNumber.leftAnchor.constraint(equalTo: posterLastSeason.rightAnchor, constant: 12),
            

            lastDateSeason.topAnchor.constraint(equalTo: seasonNumber.bottomAnchor),
            lastDateSeason.leftAnchor.constraint(equalTo: posterLastSeason.rightAnchor, constant: 12),

            castCollectionView.topAnchor.constraint(equalTo: posterLastSeason.bottomAnchor, constant: 12),
            castCollectionView.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            castCollectionView.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),
            castCollectionView.heightAnchor.constraint(equalTo: scrollContainer.heightAnchor, multiplier: 0.35),


            
            tableSeasons.topAnchor.constraint(equalTo: castCollectionView.bottomAnchor, constant: 12),
            tableSeasons.leftAnchor.constraint(equalTo: scrollContainer.leftAnchor, constant: 12),
            tableSeasons.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor, multiplier: 0.8),
            tableSeasons.heightAnchor.constraint(equalTo: scrollContainer.heightAnchor, multiplier: 0.45),
            
            
            
            
        ]
    }

    func activateCurrentLayout() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                // iPhone 5 or 5S or 5C
                NSLayoutConstraint.activate(compactConstraints)
            case 1334:
                // iPhone 6/6S/7/8
                NSLayoutConstraint.activate(regularConstraints)

            case 2208:
                // iPhone 6+/6s+/7+/8+
                NSLayoutConstraint.activate(regularConstraints)
            case 2436:
                // iPhone X/Xs/11 Pro
                NSLayoutConstraint.activate(largeConstraints)
            case 1792:
                // iPhone 11/XR
                NSLayoutConstraint.activate(largeConstraints)
            default:
                // 11 Pro Max/Xs Max
                NSLayoutConstraint.activate(largeConstraints)
            }
        } else if UIDevice().userInterfaceIdiom == .pad {
            // iPad cases
            NSLayoutConstraint.activate(regularConstraints)
        }
    }

    

}

extension SelectedShowView {
    @objc private func popular() {
        delegate?.popularBTN(self)
    }
   
    
    @objc private func goBack() {
        delegate?.dimiz(self)
    }

}



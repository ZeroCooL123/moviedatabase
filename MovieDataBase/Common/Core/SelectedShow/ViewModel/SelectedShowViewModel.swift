//
//  SelectedShowViewModel.swift
//  MovieDataBase
//
//  Created by JoseRAulCompany on 3/1/21.
//

import Foundation
protocol SelectedShowViewProtocol {
   
}

protocol SelectedShowViewModelProtocol {
    
    
}

class SelectedShowViewModel: SelectedShowViewModelProtocol {
    
    
    
   
    
    var view: SelectedShowViewProtocol
    var repository: SelectedShowRepositoryProtocol
    var defaults: DefaultsManager
    
    init(view: SelectedShowViewProtocol, repository: SelectedShowRepositoryProtocol, defaults: DefaultsManager) {
        self.view = view
        self.repository = repository
        self.defaults = defaults
    }

    
}


//
//  ExampleFactory.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

protocol ExampleFactory {
    func makeMainCoordinator(_ navigationController: UINavigationController) -> MainCoordinator
    func makeExampleViewModel(_ view: ExampleViewProtocol, repository: ExampleRepositoryProtocol) -> ExampleViewModelProtocol
    func makeExampleRepository() -> ExampleRepository
    func makeExampleView(_ bgColor: UIColor) -> ExampleView
    func makeExampleVC(_ view: ExampleView, coordinator: MainCoordinator) -> ExampleVC
}

extension DependencyContainer: ExampleFactory {
    
    func makeMainCoordinator(_ navigationController: UINavigationController) -> MainCoordinator {
        let mainCoordinator = MainCoordinator(self, navigationController: navigationController)
        
        return mainCoordinator
    }
    
    func makeExampleViewModel(_ view: ExampleViewProtocol, repository: ExampleRepositoryProtocol) -> ExampleViewModelProtocol {
        let viewModel = ExampleViewModel(view: view, repository: repository, defaults: defaults)
        
        return viewModel
    }
    
    func makeExampleRepository() -> ExampleRepository {
        return ExampleRepository(networkService: networkService, localService: localService)
    }
    
    func makeExampleView(_ bgColor: UIColor) -> ExampleView {
        let view = ExampleView()
        view.backgroundColor = bgColor
        
        return view
    }
    
    func makeExampleVC(_ view: ExampleView, coordinator: MainCoordinator) -> ExampleVC {
        let vc = ExampleVC(UI: view, factory: self, coordinator: coordinator, defaults: defaults)
        
        return vc
    }

}


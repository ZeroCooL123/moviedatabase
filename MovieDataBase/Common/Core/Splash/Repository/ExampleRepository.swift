//
//  ExampleRepository.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

protocol ExampleRepositoryProtocol: WebRepository {
}

class ExampleRepository: ExampleRepositoryProtocol {

    var localService: LocalManager
    var networkService: NetworkManager
    
    required init(networkService: NetworkManager, localService: LocalManager) {
        self.networkService = networkService
        self.localService = localService
    }

}


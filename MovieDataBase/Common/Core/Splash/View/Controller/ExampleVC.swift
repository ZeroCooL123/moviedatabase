//
//  ExampleVC.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import UIKit

class ExampleVC: UIViewController {
    
    // MARK: - Controller's properties

    let UI: ExampleView
    let coordinator: MainCoordinator
    let defaults: DefaultsManager
    private var factory: ExampleFactory
    lazy var viewModel: ExampleViewModelProtocol = {
        factory.makeExampleViewModel(self, repository: factory.makeExampleRepository())
    }()
    
    // MARK: - Controller's initializers
    
    required init(UI: ExampleView, factory: ExampleFactory, coordinator: MainCoordinator, defaults: DefaultsManager) {
        self.UI = UI
        self.factory = factory
        self.coordinator = coordinator
        self.defaults = defaults
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Controller's life cycle
    
    override func loadView() {
        self.view = self.UI
    }
    
    override func viewDidLayoutSubviews() {}

    override func viewDidLoad() {
        super.viewDidLoad()
        animate()
       
        
        
    }
    
    func animate(){
        UIView.animate(withDuration: 0,
        animations: {
            self.UI.logoMain.transform = CGAffineTransform.identity
            self.UI.poweredLogo.transform = CGAffineTransform.identity
            
        },
        completion: { _ in
            UIView.animate(withDuration: 2.5) {

                self.UI.logoMain.alpha = 1
                self.UI.poweredLogo.alpha = 1
                self.goNext()
            }
        })
    }
    
    
    
    func goNext(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.6) {
            self.coordinator.goLogin()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Memory warning!")
    }
    
    
    
}

extension ExampleVC: ExampleViewProtocol {

}

extension ExampleVC: ExampleViewDelegate {
    
    func didTapStartButton(_ view: ExampleView) {

    }
    
}


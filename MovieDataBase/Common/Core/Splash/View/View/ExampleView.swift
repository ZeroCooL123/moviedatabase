//
//  ExampleView.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

protocol ExampleViewDelegate: NSObjectProtocol {
    func didTapStartButton(_ view: ExampleView)
}

class ExampleView: UIView, ViewDelegate {

    weak var delegate: ExampleViewDelegate?
    
    var compactConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var regularConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
    var largeConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()


    let logoMain: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = .clear
        iv.alpha = 0
        iv.image = UIImage(named: "theMovieDBLogo")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let poweredLogo: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = .clear
        iv.alpha = 0
        iv.image = UIImage(named: "ApplaudoLogo")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initComponents()
    }
    
    func initComponents() {
        addComponents()
        setAutolayout()
        activateCurrentLayout()
        setDelegates()
    }
    
    func addComponents() {

        addSubview(logoMain)
        addSubview(poweredLogo)
    }
    
    func setAutolayout() {
        // Compact Constraints
        compactConstraints = [
            
            logoMain.centerYAnchor.constraint(equalTo: centerYAnchor),
            logoMain.centerXAnchor.constraint(equalTo: centerXAnchor),
            logoMain.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            logoMain.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15),
        
            poweredLogo.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            poweredLogo.centerXAnchor.constraint(equalTo: centerXAnchor),
            poweredLogo.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            poweredLogo.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15)
        ]
        
        // Regular Constraints
        regularConstraints = [
            logoMain.centerYAnchor.constraint(equalTo: centerYAnchor),
            logoMain.centerXAnchor.constraint(equalTo: centerXAnchor),
            logoMain.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            logoMain.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15),
            
            poweredLogo.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            poweredLogo.centerXAnchor.constraint(equalTo: centerXAnchor),
            poweredLogo.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            poweredLogo.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15)
        
        ]
        
        // Large Constraints
        largeConstraints = [
            logoMain.centerYAnchor.constraint(equalTo: centerYAnchor),
            logoMain.centerXAnchor.constraint(equalTo: centerXAnchor),
            logoMain.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            logoMain.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15),
        
            poweredLogo.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            poweredLogo.centerXAnchor.constraint(equalTo: centerXAnchor),
            poweredLogo.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            poweredLogo.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15)
        ]
    }
    
    func activateCurrentLayout() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                // iPhone 5 or 5S or 5C
                NSLayoutConstraint.activate(compactConstraints)
            case 1334:
                // iPhone 6/6S/7/8
                NSLayoutConstraint.activate(regularConstraints)
            case 2208:
                // iPhone 6+/6s+/7+/8+
                NSLayoutConstraint.activate(regularConstraints)
            case 2436:
                // iPhone X/Xs/11 Pro
                NSLayoutConstraint.activate(largeConstraints)
            case 1792:
                // iPhone 11/XR
                NSLayoutConstraint.activate(largeConstraints)
            default:
                // 11 Pro Max/Xs Max
                NSLayoutConstraint.activate(largeConstraints)
            }
        } else if UIDevice().userInterfaceIdiom == .pad {
            // iPad cases
            NSLayoutConstraint.activate(regularConstraints)
        }
    }

}

extension ExampleView {
    @objc private func startButton() {
        delegate?.didTapStartButton(self)
    }
}

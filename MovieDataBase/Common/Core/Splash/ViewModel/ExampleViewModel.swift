//
//  ExampleViewModel.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

protocol ExampleViewProtocol {
}

protocol ExampleViewModelProtocol {
    
}

class ExampleViewModel: ExampleViewModelProtocol {
    
    var view: ExampleViewProtocol
    var repository: ExampleRepositoryProtocol
    var defaults: DefaultsManager
    
    init(view: ExampleViewProtocol, repository: ExampleRepositoryProtocol, defaults: DefaultsManager) {
        self.view = view
        self.repository = repository
        self.defaults = defaults
    }

    
}


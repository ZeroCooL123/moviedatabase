//
//  UserDefaultsKeys.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

enum DefaultsKey {
    static let IsFirstEntry: String = "isFirstEntry"
    static let LanguageCode: String = "languageCode"
    static let PinCode: String = "pinCode"
    static let IsBiometricsSet: String = "IsBiometricsSet"
}

//
//  FontsManager.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

enum Fonts: String {
    case ReformationSansRegular = "reformation sans regular"
    case Kermesse = "kermesse"
    case AVENIR = "Avenir"
    case AVENIRBold = "Avenir-Black"
    
    func of(size: CGFloat) -> UIFont {
        let font = UIFont(name: rawValue, size: size) ?? UIFont.systemFont(ofSize: size)
        return font
    }
}



//
//  StrManager.swift
//  DeliveryMan
//
//  Created by José Raúl on 09/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

enum AppModuleKey: String {
    case Splash = "Splash"
    case Terms = "Terms"
    //AQUI AGREGAR PRIMERO
}

enum StrError: String {
    case plistNotFound = "El archivo con extensión .plist no fue encontrado"
    case enviromentNotFound = "El ambiente actual no existe en el archivo info.plist"
    case baseUrlNotFound = "La URL Base no existe en el archivo info.plist"
    case invalidURL = "La URL es inválida"
    case invalidData = "No hay data"
}

public enum AppModule {

    private static let path: String = {
        guard let path = Bundle.main.path(forResource: "\(Environment.Owner)_Strings", ofType: "plist") else { fatalError(StrError.plistNotFound.rawValue) }

        return path
    }()
    
    private static let StrURL: URL = {
        let url = URL(fileURLWithPath: AppModule.path)

        return url
    }()
    
    private static let StrData: Data = {
        guard let data = try? Data(contentsOf: AppModule.StrURL) else { fatalError(StrError.invalidData.rawValue) }
        
        return data
    }()
    
    private static let strDictionary: [String: Any] = {
        guard let plist = try? PropertyListSerialization.propertyList(from: AppModule.StrData, options: .mutableContainers, format: nil) as? [String: Any] else { fatalError(StrError.plistNotFound.rawValue) }
        
        return plist
    }()
    //AGREGAR UN MODULE COMO ESTE
    
    static let SplashModule: NSDictionary = {
        guard let currenModule = AppModule.strDictionary[AppModuleKey.Splash.rawValue] as? NSDictionary else {
            fatalError(StrError.enviromentNotFound.rawValue)
        }
        return currenModule
    }()
    
    static let TermsModule: NSDictionary = {
        guard let currenModule = AppModule.strDictionary[AppModuleKey.Terms.rawValue] as? NSDictionary else {
            fatalError(StrError.enviromentNotFound.rawValue)
        }
        return currenModule
    }()
    
}

enum StrManager {
    
    enum Splash {
        static let string1 = AppModule.SplashModule["string1"] as? String ?? ""
        static let string2 = AppModule.SplashModule["string2"] as? String ?? ""
        static let string3 = AppModule.SplashModule["string3"] as? String ?? ""
    }
    
    enum Terms {
        static let string1 = AppModule.TermsModule["string1"] as? String ?? ""
        static let string2 = AppModule.TermsModule["string2"] as? String ?? ""
        static let string3 = AppModule.TermsModule["string3"] as? String ?? ""
    }
    
    //AGREGAR UN MANAGER

}

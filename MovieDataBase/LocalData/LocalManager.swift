//
//  LocalManager.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import CoreData

enum LocalError: String, Error {
    case decodingError = "Error al decodificar el objeto"
}

protocol LocalService {
    var persistentContainer: NSPersistentContainer { get }
    
    func getDataFromJson<T>(resource: LocalResource<T>, completion: @escaping (Result<T, LocalError>) -> Void)
}

class LocalManager: LocalService {
    
    var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ArquitectureExample")
        container.loadPersistentStores { (storeDescription, err) in
            if let err = err {
                fatalError("Loading of store failed: \(err)")
            }
        }
        return container
    }()
    
    func getDataFromJson<T>(resource: LocalResource<T>, completion: @escaping (Result<T, LocalError>) -> Void) where T : Decodable, T : Encodable {
        
        guard let url = Bundle.main.url(forResource: resource.fileName, withExtension: resource.fileExtension) else { return }

        guard let data = try? Data(contentsOf: url) else { return }

        // Deserialize JSON
        let result = try? JSONDecoder().decode(T.self, from: data)
        if let result = result {
            DispatchQueue.main.async {
                completion(.success(result))
            }
        } else {
            completion(.failure(.decodingError))
        }
    }
    
}


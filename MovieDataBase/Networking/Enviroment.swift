//
//  Enviroment.swift
//  RestauranteMifel
//
//  Created by José Raúl on 13/05/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

enum PlistKey {
    static let Enviroment: String = "Enviroment"
    static let BaseURL: String = "BASE_URL"
    static let Owner: String = "OWNER"
}

enum EnviromentError: String {
    case plistNotFound = "El archivo con extensión .plist no fue encontrado"
    case enviromentNotFound = "El ambiente actual no existe en el archivo info.plist"
    case baseUrlNotFound = "La URL Base no existe en el archivo info.plist"
    case invalidURL = "La URL es inválida"
}

public enum Environment {

    private static let infoDictionary: [String: Any] = {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError(EnviromentError.plistNotFound.rawValue)
        }
        return dict
    }()
    
    static let enviroment: String = {
        guard let currentEnviroment = Environment.infoDictionary[PlistKey.Enviroment] as? String else {
            fatalError(EnviromentError.enviromentNotFound.rawValue)
        }
        return currentEnviroment
    }()
    static let Owner: String = {
        guard let currentEnviroment = Environment.infoDictionary[PlistKey.Owner] as? String else {
            fatalError(EnviromentError.enviromentNotFound.rawValue)
        }
        return currentEnviroment
    }()
    
    static let baseURL: URL = {
        guard let rootURLstring = Environment.infoDictionary[PlistKey.BaseURL] as? String else {
            fatalError(EnviromentError.baseUrlNotFound.rawValue)
        }
        guard let url = URL(string: rootURLstring) else {
            fatalError(EnviromentError.invalidURL.rawValue)
        }
        return url
    }()
    
}




enum Endpoint {
    
    
    
    static let addAccountWS: String = "/register"
     static let loginWS: String = "/oauth/token"
    static let editProfileWS: String = "/v1/user/profile"
    static let scheduleNowWS: String = "/v1/schedule/now"
    static let restorePassWS: String = "/v1/user/restorePassword"

    
}


//
//  NetworkManager.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

enum NetworkError: String, Error {
    case decodingError = "Ocurrió un error al obtener la información ser servidor"
    case nointernet = "The Internet connection appears to be offline."
    case domainError = "Ocurrió un problema con el servidor"
    case urlError = "No fue posible conectarse con el servidor, espere unos segundos y vuelva a intentar"
    case timeoutError = "Tiempo de espera agotado"
    case sinSalidaNetwork = "A server with the specified hostname could not be found."
}

enum HTTPMethods {
    static let GET: String = "GET"
    static let POST: String = "POST"
    static let PUT: String = "PUT"
    static let DELETE: String = "DELETE"
}

protocol WebService {
    func getRequest<T>(resource: Resource<T>, completion: @escaping (Result<T, NetworkError>) -> Void)
    func postRequest<T>(resource: Resource<T>, completion: @escaping (Result<T, NetworkError>) -> Void)
    func putRequest<T>(resource: Resource<T>, completion: @escaping (Result<T, NetworkError>) -> Void)
    func postDataFormRequest(resource: TokenRequest, completion: @escaping (Result<Token, NetworkError>) -> Void)
}

class NetworkManager: WebService {

    // MARK: - putRequest
    
    /**
    ## Método utilizado para realizar una petición PUT a una API

    Método  que permite realizar una petición POST  tomando en cuenta un recurso proporcionado(url - parameters), este método a través de la implementación del Result Type es capaz de devolver un objeto esperado o una respuesta de error si es el caso.

    - Note: El método es genérico y puede devolver cualquier objeto decodeado.
    - Returns: El objeto decodeado requerido.
    - parameters:
        - resource: Este objeto contiene la url y los parametros requeridos para realizar la petición.
        - completion: Obtiene la respuesta del servicio y la maneja ccual sea el caso.
    - Version: 1.0
    - Author: José Raúl
     */
    
    func putRequest<T>(resource: Resource<T>, completion: @escaping (Result<T, NetworkError>) -> Void) where T : Decodable, T : Encodable {
        
        guard let enconded = try? JSONEncoder().encode(resource.parameters) else {
            return
        }
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(resource.authorizationKey ?? "")"
        ]
        
        var request = URLRequest(url: resource.url)
        request.httpMethod = HTTPMethods.PUT
        request.timeoutInterval = 30
        request.httpBody = enconded
        request.allHTTPHeaderFields = headers
        
        let json = try? JSONSerialization.jsonObject(with: enconded, options: JSONSerialization.ReadingOptions.mutableContainers)
        //print(json ?? (Any).self)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in

            guard let data = data, error == nil else {
                completion(.failure(.domainError))
                return
            }

            let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            //print(json)
            let result = try? JSONDecoder().decode(T.self, from: data)
            if let result = result {
                DispatchQueue.main.async {
                    completion(.success(result))
                }
            } else {
                DispatchQueue.main.async {
                    completion(.failure(.decodingError))
                }
            }
        }.resume()
    }
    
    // MARK: - postRequest
    
    /**
    ## Método utilizado para realizar una petición POST a una API

    Método  que permite realizar una petición POST  tomando en cuenta un recurso proporcionado(url - parameters), este método a través de la implementación del Result Type es capaz de devolver un objeto esperado o una respuesta de error si es el caso.

    - Note: El método es genérico y puede devolver cualquier objeto decodeado.
    - Returns: El objeto decodeado requerido.
    - parameters:
        - resource: Este objeto contiene la url y los parametros requeridos para realizar la petición.
        - completion: Obtiene la respuesta del servicio y la maneja ccual sea el caso.
    - Version: 1.0
    - Author: José Raúl
     */
    
    func postRequest<T>(resource: Resource<T>, completion: @escaping (Result<T, NetworkError>) -> Void) where T : Decodable, T : Encodable {
        
        guard let enconded = try? JSONEncoder().encode(resource.parameters) else {
            return
        }
        
        let headers = [
            "Content-Type": "application/json"
        ]
        
        var request = URLRequest(url: resource.url)
        request.httpMethod = HTTPMethods.POST
        request.timeoutInterval = 30
        request.httpBody = enconded
        request.allHTTPHeaderFields = headers
        
        let json = try? JSONSerialization.jsonObject(with: enconded, options: JSONSerialization.ReadingOptions.mutableContainers)
        //print(json ?? (Any).self)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in

            let err = error?.localizedDescription
            if err == "The Internet connection appears to be offline."{
                DispatchQueue.main.async {
                    completion(.failure(.nointernet))
                }
            }
            
            if err == "The request timed out."{
                        completion(.failure(.timeoutError))
                    }
            
            
            if err == "A server with the specified hostname could not be found."{ // si esta conectado a internet pero no tiene datos
                        completion(.failure(.sinSalidaNetwork))
                    }
            guard let data = data, error == nil else {
                //completion(.failure(.domainError))
                return
            
            }

            let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            //print(json)
            let result = try? JSONDecoder().decode(T.self, from: data)
            if let result = result {
                DispatchQueue.main.async {
                    completion(.success(result))
                }
            } else {
                DispatchQueue.main.async {
                    completion(.failure(.decodingError))
                }
            }
        }.resume()
    }
    
    // MARK: - getRequest
    
    /**
    ## Método utilizado para realizar una petición GET a una API

    Método  que permite realizar una petición GET  tomando en cuenta un recurso proporcionado(url), este método a través de la implementación del Result Type es capaz de devolver un objeto esperado o una respuesta de error si es el caso.

    - Note: El método es genérico y puede devolver cualquier objeto decodeado.
    - Returns: El objeto decodeado requerido.
    - parameters:
        - resource: Este objeto contiene la url requeridA para realizar la petición.
        - completion: Obtiene la respuesta del servicio y la maneja ccual sea el caso.
    - Version: 1.0
    - Author: José Raúl
     */

    func getRequest<T>(resource: Resource<T>, completion: @escaping (Result<T, NetworkError>) -> Void) where T : Decodable, T : Encodable {
        
//        let headers = [
//            "api_key": "8d9ced07152c868de3f180fef43510a7"
//        ]
        
        var request = URLRequest(url: resource.url)
        request.httpMethod = HTTPMethods.GET
        request.timeoutInterval = 30
        //request.allHTTPHeaderFields = headers
        
        let semaphore = DispatchSemaphore(value: 0)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in

            let err = error?.localizedDescription
            if err == "The Internet connection appears to be offline."{
                DispatchQueue.main.async {
                    completion(.failure(.nointernet))
                }
            }
            
            if err == "The request timed out."{
                        completion(.failure(.timeoutError))
                    }
            
            
            if err == "A server with the specified hostname could not be found."{ // si esta conectado a internet pero no tiene datos
                        completion(.failure(.sinSalidaNetwork))
                    }
            guard let data = data, error == nil else {
                //completion(.failure(.domainError))
                return
            }
            


            let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            //print(json)
            let result = try? JSONDecoder().decode(T.self, from: data)
            if let result = result {
                DispatchQueue.main.async {
                    completion(.success(result))
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(.failure(.decodingError))
                }
            }
            semaphore.signal()
        }.resume()
        
        
        
        
    }
    
    
    
    
    // MARK: - For dataForm
    
    func postDataFormRequest(resource: TokenRequest, completion: @escaping (Result<Token, NetworkError>) -> Void) {
        
        let parameters = [
            [
                "key": "grant_type",
                "value": resource.grant_type,
                "type": "text"
            ],
            [
                "key": "username",
                "value": resource.username,
                "type": "text"
            ],
            [
                "key": "password",
                "value": resource.password,
                "type": "text"
        
            ]
        ]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        
        for param in parameters {
            if param["disabled"] == nil {
                let paramName = param["key"] ?? ""
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                let paramType = param["type"] ?? ""
                
                if paramType == "text" {
                    let paramValue = param["value"] ?? ""
                    body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                    let paramSrc = param["src"] ?? ""
                
                    if let fileData = try? NSData(contentsOfFile: paramSrc, options: .alwaysMapped) {
                        let fileContent = String(data: fileData as Data, encoding: .utf8) ?? ""
                        body += "; filename=\"\(paramSrc)\"\r\n" + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                    }
                }
            }
        }
        
        body += "--\(boundary)--\r\n";
        
        let postData = body.data(using: .utf8)
        
        guard let url = URL(string: "http://3.216.253.237:9000/oauth/token") else { return }

        var request = URLRequest(url: url,timeoutInterval: Double.infinity)
        request.addValue("Basic Ymx1bW9uX3BheV9yZXN0YXVyYW50X2FwaTpibHVtb25fcGF5X3Jlc3RhdXJhbnRfYXBpX3Bhc3N3b3Jk", forHTTPHeaderField: "Authorization")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethods.POST
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                completion(.failure(.domainError))
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            //print(json)

            let result = try? JSONDecoder().decode(Token.self, from: data)
            
            if let result = result {
                DispatchQueue.main.async {
                    completion(.success(result))
                }
            } else {
                completion(.failure(.decodingError))
            }
        }

        task.resume()
    }
    
}



//
//  NetworkResource.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

struct Resource<T: Codable> {
    let url: URL
    let parameters: AnyEncodable?
    let authorizationKey: String?
    
    init(url: URL, parameters: AnyEncodable? = nil, authorizationKey: String?) {
        self.url = url
        self.parameters = parameters
        self.authorizationKey = authorizationKey
    }
}

struct AnyEncodable: Encodable {
    let value: Encodable

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try value.encode(to: &container)
    }
}

extension Encodable {
    func encode(to container: inout SingleValueEncodingContainer) throws {
        try container.encode(self)
    }
}


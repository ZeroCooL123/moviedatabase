//
//  Token.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

struct Token: Initiable {
    
    let access_token: String
    let token_type: String
    let refresh_token: String
    let expires_in: Int
    let scope: String
    let user_entity: Int
    let user_id: Int
    let user_system: Int
    let jti: String
    
    init() {
        self.access_token = ""
        self.token_type = ""
        self.refresh_token = ""
        self.expires_in = 0
        self.scope = ""
        self.user_entity = 0
        self.user_id = 0
        self.user_system = 0
        self.jti = ""
    }
}

struct TokenRequest: Initiable {
    
    var grant_type: String = ""
    var username: String = ""
    var password: String = ""
    
    init() {}
    
    init(grand_type: String, username: String, password: String) {
        self.grant_type = grand_type
        self.username = username
        self.password = password
    }
}


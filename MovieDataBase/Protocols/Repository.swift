//
//  Repository.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation

// MARK: - Base repository

protocol Repository {
    init(networkService: NetworkManager, localService: LocalManager)
}

// MARK: - Web Repository

protocol WebRepository: Repository {
    
    var networkService: NetworkManager { get set }
    
}

// MARK: - Local repository

protocol LocalRepository: Repository {
    
    var localService: LocalManager { get set }
    
}


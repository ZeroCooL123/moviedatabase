//
//  ViewDelegate.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

protocol ViewDelegate {
    var compactConstraints: [NSLayoutConstraint] { get set }
    var regularConstraints: [NSLayoutConstraint] { get set }
    var largeConstraints: [NSLayoutConstraint] { get set }
    
    func initComponents()
    func addComponents()
    func setAutolayout()
    func activateCurrentLayout()
    func setDelegates()
    func setButtonTargets()
}

extension ViewDelegate {
    func setDelegates() {}
    func setButtonTargets() {}
}

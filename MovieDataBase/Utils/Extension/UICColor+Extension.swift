//
//  UICColor+Extension.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let main: UIColor = UIColor(named: "Main") ?? UIColor.clear
//    static let main: UIColor = UIColor(red: 226/255, green: 176/255, blue: 82/255, alpha: 1)
    
    
    
    // MARK: - Applaudo Colors
    static var mainDark: UIColor = UIColor(red: 26/255, green: 40/255, blue: 46/255, alpha: 1)
    static var algaeGreen: UIColor = UIColor(red: 33/255, green: 205/255, blue: 101/255, alpha: 1)
    static var greenBlue: UIColor = UIColor(red: 1/255, green: 210/255, blue: 119/255, alpha: 1)
    

    
}



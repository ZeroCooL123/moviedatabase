//
//  LoaderVC.swift
//  ArquitectureExample
//
//  Created by José Raúl on 29/07/20.
//  Copyright © 2020 JoseRAulCompany. All rights reserved.
//

import UIKit

class LoaderVC: UIViewController {
    
    private var UI: UIView?
    
    required init(UI: LoaderView) {
        super.init(nibName: nil, bundle: nil)
        
        self.UI = UI
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = self.UI
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
